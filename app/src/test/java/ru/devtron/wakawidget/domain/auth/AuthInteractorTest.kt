package ru.devtron.wakawidget.domain.auth

import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.mock
import ru.devtron.wakawidget.data.entity.res.TokenDataRes
import ru.devtron.wakawidget.data.repos.AuthRepo
import ru.devtron.wakawidget.data.server.Result

/**
 * Created on 03/10/2018 by ruslanaliev.
 */
class AuthInteractorTest {

    private lateinit var interactor: AuthInteractor
    private lateinit var authRepo: AuthRepo

    private val HASH = "some_hash_here"
    private val OAUTH_PARAMS = OAuthParams("appId", "appKey", "redirect_url", "scopes")

    @Before
    fun setUp() {
        authRepo = mock(AuthRepo::class.java)
        interactor = AuthInteractor(authRepo, HASH, OAUTH_PARAMS)
    }

    @Test
    fun check_oauth_redirect() {
        val testUrl = OAUTH_PARAMS.redirectUrl + "somepath"
        val result = interactor.checkOAuthRedirect(testUrl)
        assertTrue(result)
    }

    @Test
    fun check_oauth_bad_redirect_path() {
        val testUrl = "app://otherUrl/somepath"
        val result = interactor.checkOAuthRedirect(testUrl)
        assertFalse(result)
    }

    @Test
    fun check_logout_cleans_token() {
        interactor.logout()
        verify(authRepo).clearAuthData()
    }

    @Test
    fun refresh_token_correct_oauth() {
        val code = "helloReader"
        val testUrl = "http://something.com/test?code=$code&state=happiness$HASH"
        val accessToken = "whateverToken"
        val whateverUserId = "whateverUserId"
        val tokenData = TokenDataRes(accessToken, 0.0, "", "", "", whateverUserId)

        runBlocking {
            whenever(authRepo.requestOAuthToken(
                    ArgumentMatchers.anyString(),
                    ArgumentMatchers.anyString(),
                    ArgumentMatchers.anyString(),
                    ArgumentMatchers.anyString())).thenReturn(Result.Success(tokenData))

            interactor.login(testUrl)

            verify(authRepo).saveAuthData(accessToken, whateverUserId)
        }
    }

    @Test
    fun refresh_token_incorrect_oauth() {
        val testOauthRedirect = "There is no token"

        runBlocking {
            interactor.login(testOauthRedirect)
            verifyNoMoreInteractions(authRepo)
        }
    }

    @Test
    fun is_signed_in() {
        whenever(authRepo.isSignedIn).thenReturn(true)
        val result = interactor.isSignedIn()

        verify(authRepo).isSignedIn
        assertTrue(result)
    }

    @Test
    fun is_not_signed_in() {
        whenever(authRepo.isSignedIn).thenReturn(false)
        val result = interactor.isSignedIn()

        verify(authRepo).isSignedIn
        assertFalse(result)
    }
}
