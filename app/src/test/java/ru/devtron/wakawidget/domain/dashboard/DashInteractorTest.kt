package ru.devtron.wakawidget.domain.dashboard

import com.nhaarman.mockito_kotlin.*
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.mockito.Mockito.mock
import ru.devtron.wakawidget.data.entity.res.*
import ru.devtron.wakawidget.data.repos.DashRepo
import ru.devtron.wakawidget.data.server.Result
import ru.devtron.wakawidget.framework.ResourceManager

/**
 * Created on 03/10/2018 by ruslanaliev.
 */
class DashInteractorTest {

    private lateinit var interactor: DashInteractor
    private lateinit var dashRepo: DashRepo

    @Before
    fun setUp() {
        dashRepo = mock(DashRepo::class.java)
        val resourceManager = mock(ResourceManager::class.java)
        whenever(resourceManager.getQuanityString(any(), any())).thenReturn("")
        interactor = DashInteractor(dashRepo)
    }

    @Test
    fun refresh_data_saving_successful() {
        runBlocking {

            val mockStatsData = StatsWrapperRes(
                StatsRes(
                    BestDayRes(null, null), 234,
                    emptyList(), emptyList(), emptyList(), emptyList(), "", 21f
                )
            )

            whenever(
                dashRepo.getCurrentUserStats(
                    ArgumentMatchers.anyString()
                )
            ).thenReturn(Result.Success(mockStatsData))

            whenever(
                dashRepo.getDuration(
                    ArgumentMatchers.anyString()
                )
            ).thenReturn(
                Result.Success(
                    DurationWrapperRes(
                        listOf(
                            DurationRes(4.0)
                        )
                    )
                )
            )

            whenever(dashRepo.getGoals()).thenReturn(
                Result.Success(
                    GoalsWrapperRes(
                        listOf(
                            GoalsDataRes(listOf(ChartDataRes(100f, 25200f)))
                        )
                    )
                )
            )

            interactor.reloadFromNetwork()

            verify(dashRepo).saveStats(any())
        }
    }

    @Test
    fun refresh_data_error() {
        runBlocking {
            whenever(
                dashRepo.getCurrentUserStats(
                    ArgumentMatchers.anyString()
                )
            ).thenReturn(Result.Error(mock()))

            whenever(
                dashRepo.getDuration(
                    ArgumentMatchers.anyString()
                )
            ).thenReturn(Result.Error(mock()))

            whenever(dashRepo.getGoals()).thenReturn(Result.Error(mock()))

            interactor.reloadFromNetwork()

            Mockito.verify(dashRepo, never()).saveStats(any())
        }
    }
}
