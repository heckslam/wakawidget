package ru.devtron.wakawidget.domain.dashboard.models

import android.graphics.Color
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StatsItem(
    val hours: Int?,
    val minutes: Int?,
    val name: String?,
    val percent: Double,
    val totalSeconds: Int?,
    var color: Int = Color.TRANSPARENT
)
