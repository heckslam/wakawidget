package ru.devtron.wakawidget.domain.dashboard.models

import ru.devtron.wakawidget.data.entity.res.DurationRes

data class StatsModel(
    val bestDay: BestDay?,
    val dailyAverage: Int,
    val totalSec: Int,
    val projects: List<StatsItem>?,
    val languages: List<StatsItem>?,
    val editors: List<StatsItem>?,
    val operatingSystems: List<StatsItem>?,
    val range: String,
    val lastUpdated: Long,
    val isEmpty: Boolean,
    val durations: List<DurationRes>,
    val improvementPercent: Double = 0.0,
    val rank: Int?
) {
    val todayDuration: Int
        get() = durations.sumByDouble { it.duration }.toInt()
}
