package ru.devtron.wakawidget.domain.auth

import ru.devtron.wakawidget.data.entity.res.TokenDataRes
import ru.devtron.wakawidget.data.repos.AuthRepo
import java.net.URI
import java.util.*
import javax.inject.Inject

private const val PARAMETER_CODE = "code"

class AuthInteractor(
    private val authRepository: AuthRepo,
    private val state: String,
    private val oauthParams: OAuthParams
) {

    @Inject
    constructor(
        authRepository: AuthRepo,
        oauthParams: OAuthParams
    ) : this(
        authRepository,
        UUID.randomUUID().toString(),
        oauthParams
    )

    val oauthUrl = "https://wakatime.com/oauth/authorize?client_id=${oauthParams.appId}" +
            "&redirect_uri=${oauthParams.redirectUrl}&response_type=code&state=$state&scope=${oauthParams.scopes}"

    fun checkOAuthRedirect(url: String) = url.indexOf(oauthParams.redirectUrl) == 0

    fun isSignedIn() = authRepository.isSignedIn

    suspend fun login(oauthRedirect: String) {
        if (oauthRedirect.contains(state)) {
            val tokenDataRes: TokenDataRes =
                authRepository.requestOAuthToken(
                    getQueryParameterFromUri(oauthRedirect, PARAMETER_CODE),
                    oauthParams.redirectUrl,
                    oauthParams.appId,
                    oauthParams.appKey
                )
            authRepository.saveAuthData(tokenDataRes.accessToken, tokenDataRes.uid)
        }
    }

    fun logout() = authRepository.clearAuthData()

    private fun getQueryParameterFromUri(url: String, queryName: String): String {
        val uri = URI(url)
        val query = uri.query
        val parameters = query.split("&")

        var code = ""
        for (parameter in parameters) {
            if (parameter.startsWith(queryName)) {
                code = parameter.substring(queryName.length + 1)
                break
            }
        }
        return code
    }
}
