package ru.devtron.wakawidget.domain.dashboard.models

import com.squareup.moshi.JsonClass


data class LeaderBoards(
    val name: String,
    val leaders: List<Leader>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LeaderBoards

        if (name != other.name) return false

        return true
    }

    override fun hashCode() = name.hashCode() * 31
}

@JsonClass(generateAdapter = true)
data class Leader(
    val rank: Int,
    val runningTotal: Int,
    val userName: String
)
