package ru.devtron.wakawidget.domain.dashboard

import kotlinx.coroutines.flow.mapNotNull
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import ru.devtron.wakawidget.data.repos.DashRepo
import ru.devtron.wakawidget.domain.dashboard.models.LeaderBoards
import ru.devtron.wakawidget.domain.dashboard.models.StatsModel
import ru.devtron.wakawidget.presentation.dashboard.RangeType
import javax.inject.Inject

class DashInteractor @Inject constructor(private val dashRepo: DashRepo) {

    suspend fun reload() {
        dashRepo.getCurrentUserStats(RangeType.RANGE_7_DAYS.toString(), getCurrentDate())
        dashRepo.getPrivateLeaderBoards()
    }

    fun getCachedStats() = dashRepo.getCachedStats().mapNotNull {
        it?.let {
            with(it.stats) {
                StatsModel(
                    bestDay,
                    dailyAverage,
                    totalSec,
                    projects,
                    languages,
                    editors,
                    operatingSystems,
                    range,
                    lastUpdated,
                    isEmpty,
                    durations,
                    improvementPercent,
                    rank
                )
            }
        }
    }

    fun getCachedLeads() = dashRepo.getCachedLeads().mapNotNull { leaderBoardsEntity ->
        leaderBoardsEntity?.leaderBoards?.map {
            LeaderBoards(it.name, it.leaders)
        }
    }

    private fun getCurrentDate() = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)


}
