package ru.devtron.wakawidget.domain.dashboard.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BestDay(
    val date: String,
    val time: Int
)
