package ru.devtron.wakawidget.domain.auth

data class OAuthParams(
    val appId: String,
    val appKey: String,
    val redirectUrl: String,
    val scopes: String
)