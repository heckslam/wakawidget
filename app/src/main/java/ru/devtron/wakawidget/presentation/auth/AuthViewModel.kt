package ru.devtron.wakawidget.presentation.auth

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import ru.devtron.wakawidget.domain.auth.AuthInteractor
import ru.devtron.wakawidget.framework.Resource
import ru.devtron.wakawidget.framework.arch.BaseViewModel
import ru.devtron.wakawidget.presentation.common.ErrorHandler
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(
    private val authInteractor: AuthInteractor,
    private val errorHandler: ErrorHandler
) : BaseViewModel() {

    val oauthUrlEvent = Event<Resource<String>>()
    val goToDashboardEvent = Event<Unit>()

    init {
        oauthUrlEvent.call(Resource.Success(authInteractor.oauthUrl))
    }

    fun onRedirect(url: String): Boolean {
        return if (authInteractor.checkOAuthRedirect(url)) {
            requestToken(url)
            true
        } else {
            oauthUrlEvent.call(Resource.Success(url))
            false
        }
    }

    private fun requestToken(url: String) {
        oauthUrlEvent.call(Resource.Loading)

        viewModelScope.launch {
            authInteractor.login(url)
        }.invokeOnCompletion {
            if (it == null) {
                goToDashboardEvent.call()
            } else {
                errorHandler.proceed(it)
            }
        }
    }
}
