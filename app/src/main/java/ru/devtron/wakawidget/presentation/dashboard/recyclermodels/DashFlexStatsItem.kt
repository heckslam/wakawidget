package ru.devtron.wakawidget.presentation.dashboard.recyclermodels

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.item_stats_entity.view.*
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.domain.dashboard.models.StatsItem
import ru.devtron.wakawidget.framework.extensions.fromDimen
import ru.devtron.wakawidget.framework.extensions.inflate

private const val TOTAL_PERCENT = 100f
private const val BIG_STAT_ITEM = 30
private const val MEDIUM_STAT_ITEM_MIN = 6.0
private const val MEDIUM_STAT_ITEM_MAX = 8.0
private const val STAT_ITEM_SPAN_COUNT = 2
private const val MAX_STAT_ITEMS_TO_DISPLAY = 5
private const val BIG_STAT_ITEM_OFFSET = 0.03f
private const val MEDIUM_STAT_ITEM_OFFSET = 0.02f

@ModelView(defaultLayout = R.layout.item_stats_entity)
class DashFlexStatsItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    @TextProp
    fun setHeader(text: CharSequence) {
        header.text = text
    }

    @ModelProp
    fun setChart(statsItems: List<StatsItem>) {
        statsCardRecyclerView.layoutManager =
            GridLayoutManager(context, STAT_ITEM_SPAN_COUNT, RecyclerView.VERTICAL, false)
        statsContainer.removeAllViews()

        statsCardRecyclerView.withModels {
            statsItems.take(MAX_STAT_ITEMS_TO_DISPLAY).forEach {
                val flexParams = FlexboxLayout.LayoutParams(
                    FlexboxLayout.LayoutParams.WRAP_CONTENT,
                    FlexboxLayout.LayoutParams.WRAP_CONTENT
                )
                val percentInLayout = (it.percent / TOTAL_PERCENT).toFloat()
                flexParams.marginStart = context.fromDimen(R.dimen.margin_xxs)
                flexParams.flexBasisPercent = when {
                    it.percent > BIG_STAT_ITEM -> percentInLayout - BIG_STAT_ITEM_OFFSET
                    it.percent in MEDIUM_STAT_ITEM_MIN..MEDIUM_STAT_ITEM_MAX ->
                        percentInLayout + MEDIUM_STAT_ITEM_OFFSET
                    else -> percentInLayout
                }
                val view = statsContainer.inflate(R.layout.item_stat_progress)
                (view as TextView).text = context.getString(R.string.percent_format_int, it.percent)
                view.layoutParams = flexParams
                view.background.mutate().setTint(it.color)
                statsContainer.addView(view)

                chartColoredItem {
                    id(it.hashCode())
                    labelColor(it.color)
                    header(it.name ?: "")
                }
            }
        }
    }
}
