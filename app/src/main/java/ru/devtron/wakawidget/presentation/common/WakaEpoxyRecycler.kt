package ru.devtron.wakawidget.presentation.common

import android.content.Context
import android.util.AttributeSet
import com.airbnb.epoxy.EpoxyRecyclerView

class WakaEpoxyRecycler @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : EpoxyRecyclerView(context, attrs, defStyleAttr) {
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        adapter = null
    }
}
