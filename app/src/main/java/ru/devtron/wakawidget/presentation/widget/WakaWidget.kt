package ru.devtron.wakawidget.presentation.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.devtron.wakawidget.BuildConfig
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.data.local.AppDatabase
import ru.devtron.wakawidget.framework.WorkerHelper
import ru.devtron.wakawidget.framework.di.buildDatabase
import ru.devtron.wakawidget.framework.extensions.toHumanReadableTime
import ru.devtron.wakawidget.framework.extensions.toHumanReadableTimeShort
import ru.devtron.wakawidget.framework.logAndSaveToFile
import ru.devtron.wakawidget.presentation.main.MainActivity
import java.text.SimpleDateFormat
import java.util.*

/**
 * Implementation of App Widget functionality.
 */
class WakaWidget : AppWidgetProvider() {

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
        WorkerHelper.updateWorkerWithConstraints(context = context)
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
        WorkerHelper.cancelWork()
    }

    companion object {

        lateinit var appDatabase: AppDatabase

        const val ACTION_FORCE_UPDATE = "ru.devtron.wakawidget.force_update"

        fun updateAppWidget(
            context: Context,
            appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            val widgetView = RemoteViews(context.packageName, R.layout.waka_widget)

            logAndSaveToFile("updateAppWidget called ${getCurrentTimeUsingDate()}")

            if (::appDatabase.isInitialized.not()) appDatabase = buildDatabase(context)

            GlobalScope.launch {
                val statsEntity = appDatabase.statsDao().getStats()
                val statsModel = statsEntity?.stats
                statsModel?.let {
                    logAndSaveToFile("fun updateAppWidget lastUpdated  ${statsEntity.lastUpdatedTime}")
                    widgetView.setTextViewText(
                        R.id.todayStatsTv,
                        statsModel.todayDuration.toHumanReadableTimeShort(context)
                    )
                    widgetView.setTextViewText(
                        R.id.dailyAverage,
                        statsModel.dailyAverage.toHumanReadableTime(context)
                    )
                    widgetView.setTextViewText(
                        R.id.thisWeek,
                        statsModel.totalSec.toHumanReadableTimeShort(context)
                    )
                    if (BuildConfig.DEBUG) {
                        widgetView.setTextViewText(
                            R.id.tvThisWeekTitle,
                            context.getString(
                                R.string.stats_header_weekly_with_time,
                                statsEntity.lastUpdatedTime
                            )
                        )
                    }
                    widgetView.setTextViewText(
                        R.id.percentImprovementTv,
                        context.getString(
                            R.string.percent_format_int,
                            statsModel.improvementPercent
                        )
                    )
                    // Instruct the widget manager to update the widget
                    appWidgetManager.updateAppWidget(appWidgetId, widgetView)
                }
            }

            // on widget click - just open app
            val launcherIntent = Intent(context, MainActivity::class.java)
            val pIntent = PendingIntent.getActivity(context, appWidgetId, launcherIntent, 0)
            widgetView.setOnClickPendingIntent(R.id.widgetContainer, pIntent)

            // Force update widget
            val updateIntent = Intent(context, WakaWidget::class.java)
            updateIntent.action = ACTION_FORCE_UPDATE
            val pIntent2 = PendingIntent.getBroadcast(context, appWidgetId, updateIntent, 0)
            widgetView.setOnClickPendingIntent(R.id.forceUpdate, pIntent2)
        }

        private fun getCurrentTimeUsingDate(): String? {
            val date = Date()
            val strDateFormat = "hh:mm:ss a"
            val dateFormat = SimpleDateFormat(strDateFormat, Locale.getDefault())
            return dateFormat.format(date)
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        intent?.action?.let {
            logAndSaveToFile("Caught action $it")
            if (it.equals(ACTION_FORCE_UPDATE, ignoreCase = true)) {
                // create separate worker for force update
                WorkerHelper.updateWorkerWithConstraints(context!!)
            }
        }
    }
}
