package ru.devtron.wakawidget.presentation.dashboard

import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import ru.devtron.wakawidget.data.local.User
import ru.devtron.wakawidget.domain.auth.AuthInteractor
import ru.devtron.wakawidget.domain.dashboard.DashInteractor
import ru.devtron.wakawidget.framework.Resource
import ru.devtron.wakawidget.framework.WorkerHelper
import ru.devtron.wakawidget.framework.arch.BaseViewModel
import ru.devtron.wakawidget.framework.log
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(
    // Repos, UseCases and other dependecies go here
    private val dashInteractor: DashInteractor,
    private val authInteractor: AuthInteractor
) : BaseViewModel() {
    val dataState: State<DashboardViewState> = State(DashboardViewState())
    val showAddToAutoStartupDialog: Event<Unit> = Event()

    init {
        subscribeToStatsLocalDataChanges()
        subscribeToLeadersLocalDataChanges()
        reload()
        WorkerHelper.updateWorkerWithConstraints()

        if (User.dialogAutoStartAppShown.not()) {
            User.dialogAutoStartAppShown = true
            showAddToAutoStartupDialog.call()
        }
    }

    private fun subscribeToLeadersLocalDataChanges() {
        viewModelScope.launch {
            dashInteractor.getCachedLeads().collect {
                dataState.setValue(
                    dataState.valueNonNull.copy(
                        leaders = Resource.Success(it)
                    )
                )
            }
        }
    }

    private fun subscribeToStatsLocalDataChanges() {
        viewModelScope.launch {
            dashInteractor.getCachedStats().collect {
                dataState.setValue(
                    dataState.valueNonNull.copy(
                        data = if (it.isEmpty) Resource.Empty else
                            Resource.Success(it)
                    )
                )
            }
        }
    }

    fun reload() {
        viewModelScope.launch {
            try {
                dashInteractor.reload()
            } catch (e: Exception) {
                log("exceepetion ${e.message})")
                e.printStackTrace()
            }
        }
    }

    fun logout() = authInteractor.logout()
}
