package ru.devtron.wakawidget.presentation.settings

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_settings.*
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.data.local.Worker
import ru.devtron.wakawidget.framework.WorkerHelper
import ru.devtron.wakawidget.presentation.settings.recyclermodels.settingsInterval
import ru.devtron.wakawidget.presentation.settings.recyclermodels.settingsSeparatorItem
import ru.devtron.wakawidget.presentation.settings.recyclermodels.settingsSwitchItem

const val WORKER_DELAY_ID = 102

class SettingsFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_settings, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.withModels {
            settingsInterval {
                id(WORKER_DELAY_ID)
                initialProgress(Worker.workerDelay)
                changeListener { Worker.workerDelay = it }
            }

            settingsSeparatorItem {
                id(R.string.constraints)
                title(R.string.constraints)
            }

            settingsSwitchItem {
                id(R.string.wifi_on)
                title(R.string.wifi_on)
                icon(R.drawable.ic_wifi_black_24dp)
                initialEnabled(Worker.wifiOnly)
                clickListener { Worker.wifiOnly = it }
            }

            settingsSwitchItem {
                id(R.string.charging)
                title(R.string.charging)
                icon(R.drawable.ic_battery_charging_full_black_24dp)
                initialEnabled(Worker.charginOnly)
                clickListener { Worker.charginOnly = it }
            }

            settingsSwitchItem {
                id(R.string.batter_not_low)
                title(R.string.batter_not_low)
                icon(R.drawable.ic_battery_20_black_24dp)
                initialEnabled(Worker.batteryNotLow)
                clickListener { Worker.batteryNotLow = it }
            }

            settingsSwitchItem {
                id(R.string.device_idle)
                title(R.string.device_idle)
                icon(R.drawable.ic_moon)
                initialEnabled(Worker.deviceIdle)
                clickListener { Worker.deviceIdle = it }
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        WorkerHelper.cancelWork()
        WorkerHelper.updateWorkerWithConstraints()
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), theme)
}
