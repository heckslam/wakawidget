package ru.devtron.wakawidget.presentation.dashboard.recyclermodels

import android.content.Context
import android.util.AttributeSet
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.item_best_day.view.*
import ru.devtron.wakawidget.R

@ModelView(defaultLayout = R.layout.item_best_day)
class DashBestDayItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    @TextProp
    fun setDate(text: CharSequence) {
        bestday_textview_date.text = text
    }

    @TextProp
    fun setTime(text: CharSequence) {
        bestday_textview_time.text = text
    }
}