package ru.devtron.wakawidget.presentation.about

import dagger.hilt.android.lifecycle.HiltViewModel
import ru.devtron.wakawidget.framework.arch.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class AboutAppViewModel @Inject constructor() : BaseViewModel()
