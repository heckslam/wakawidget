package ru.devtron.wakawidget.presentation.settings.recyclermodels

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import kotlinx.android.synthetic.main.settings_item_separator.view.*
import ru.devtron.wakawidget.R

@ModelView(defaultLayout = R.layout.settings_item_separator)
class SettingsSeparatorItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    @TextProp
    fun setTitle(text: CharSequence) {
        title.text = text
    }
}