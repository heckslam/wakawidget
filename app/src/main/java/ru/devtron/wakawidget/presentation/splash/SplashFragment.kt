package ru.devtron.wakawidget.presentation.splash

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.presentation.common.BaseFragment

@AndroidEntryPoint
class SplashFragment : BaseFragment(R.layout.fragment_splash) {
    override val viewModel: SplashViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.goToDashboardEvent.subscribe {
            navController.navigate(R.id.action_splashFragment_to_dashboardFragment)
        }
        viewModel.goToAuthEvent.subscribe {
            navController.navigate(R.id.action_splashFragment_to_authFragment)
        }
    }
}
