package ru.devtron.wakawidget.presentation.dashboard.recyclermodels

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import kotlinx.android.synthetic.main.item_average_week.view.*
import ru.devtron.wakawidget.R

@ModelView(defaultLayout = R.layout.item_average_week)
class DashAverageWeekItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    @TextProp
    fun setTotalHours(text: CharSequence) {
        weeklyStats.text = text
    }

    @TextProp
    fun setDailyAverage(text: CharSequence) {
        dailyAverageStats.text = text
    }
}