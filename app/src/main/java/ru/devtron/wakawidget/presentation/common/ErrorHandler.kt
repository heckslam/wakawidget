package ru.devtron.wakawidget.presentation.common

import retrofit2.HttpException
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.domain.auth.AuthInteractor
import ru.devtron.wakawidget.framework.ResourceManager
import ru.devtron.wakawidget.framework.log
import java.io.IOException
import javax.inject.Inject

class ErrorHandler @Inject constructor(
    private val authInteractor: AuthInteractor,
    private val resourceManager: ResourceManager
) {

    fun proceed(error: Throwable, messageListener: (String) -> Unit = {}) {
        error.printStackTrace()
        log("Error: $error")
        if (error is HttpException && error.code() == 401) {
            logout()
        } else {
            messageListener(error.userMessage(resourceManager))
        }
    }

    private fun logout() {
        authInteractor.logout()
        // todo open auth screen
    }


    private fun Throwable.userMessage(resourceManager: ResourceManager) = when (this) {
        is HttpException -> when (this.code()) {
            304 -> resourceManager.getString(R.string.not_modified_error)
            400 -> resourceManager.getString(R.string.bad_request_error)
            401 -> resourceManager.getString(R.string.unauthorized_error)
            403 -> resourceManager.getString(R.string.forbidden_error)
            404 -> resourceManager.getString(R.string.not_found_error)
            405 -> resourceManager.getString(R.string.method_not_allowed_error)
            409 -> resourceManager.getString(R.string.conflict_error)
            422 -> resourceManager.getString(R.string.unprocessable_error)
            500 -> resourceManager.getString(R.string.server_error_error)
            else -> resourceManager.getString(R.string.unknown_error)
        }
        is IOException -> resourceManager.getString(R.string.network_error)
        else -> resourceManager.getString(R.string.unknown_error)
    }

}
