package ru.devtron.wakawidget.presentation.dashboard

import ru.devtron.wakawidget.domain.dashboard.models.LeaderBoards
import ru.devtron.wakawidget.domain.dashboard.models.StatsModel
import ru.devtron.wakawidget.framework.Resource

data class DashboardViewState(
    // Resources go here
    val data: Resource<StatsModel> = Resource.Loading,
    val leaders: Resource<List<LeaderBoards>> = Resource.Loading
) {
    // Combinations of states above may produce more complex states
    // Functions to determine these combinations go here
}
