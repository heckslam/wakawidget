package ru.devtron.wakawidget.presentation.dashboard.recyclermodels

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import kotlinx.android.synthetic.main.item_stats_error.view.*
import ru.devtron.wakawidget.R

@ModelView(defaultLayout = R.layout.item_stats_error)
class DashErrorOrEmptyStateItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    @TextProp(defaultRes = R.string.error_state_title)
    fun setTitleText(text: CharSequence) {
        emptyTitle.text = text
    }

    @TextProp(defaultRes = R.string.error_state_subtitle)
    fun setDescription(text: CharSequence) {
        emptyDescTv.text = text
    }

    @TextProp(defaultRes = R.string.error_state_retry)
    fun setButtonText(text: CharSequence) {
        actionBtn.text = text
    }

    @CallbackProp
    fun clickListener(@Nullable onClickListener: (() -> Unit)?) {
        onClickListener?.let { listener ->
            actionBtn.setOnClickListener { listener.invoke() }
        }
    }
}