package ru.devtron.wakawidget.presentation.dashboard.recyclermodels

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.item_today_stats.view.*
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.data.entity.res.DurationRes
import ru.devtron.wakawidget.framework.extensions.color

@ModelView(defaultLayout = R.layout.item_today_stats)
class DashTodayStatsItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    @TextProp
    fun setTodayStats(text: CharSequence) {
        statsTodayTv.text = text
    }

    @ModelProp
    fun setPercent(percent: Double) {
        percentImprovementTv.text = context.getString(R.string.percent_format_int, percent)
        if (percent > 15) {
            percentImprovementTv.setCompoundDrawablesWithIntrinsicBounds(null, null, ContextCompat.getDrawable(context, R.drawable.ic_arrow_up), null)
            percentImprovementTv.setTextColor(context.color(R.color.acceptedGreen))
        } else {
            percentImprovementTv.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            percentImprovementTv.setTextColor(context.color(R.color.error_red))
        }
    }

    @ModelProp
    fun setLeaderBoardRank(rank: Int?) {
        rank?.let {
            leaderBoardTv.text = context.getString(R.string.public_leader_board_rank, it)
            leaderBoardTv.animate()
                    .alpha(1f)
                    .start()
        }
    }

    @ModelProp
    fun setChartData(durations: List<DurationRes>) {
        setupChartStyle()
        setData(durations)
    }

    private fun setData(durations: List<DurationRes>) {
        val values = ArrayList<Entry>()

        for (i in durations.indices) {
            values.add(Entry(i.toFloat(), (durations[i].duration).toFloat()))
        }

        val lineDataSet: LineDataSet

        if (todayStatsChart.data != null && todayStatsChart.data.dataSetCount > 0) {
            lineDataSet = todayStatsChart.data.getDataSetByIndex(0) as LineDataSet
            lineDataSet.values = values
            todayStatsChart.data.notifyDataChanged()
            todayStatsChart.notifyDataSetChanged()
        } else {
            // create a dataset and give it a type
            lineDataSet = LineDataSet(values, "")

            lineDataSet.setDrawIcons(false)

            lineDataSet.color = context.color(R.color.colorAccent)
            lineDataSet.lineWidth = 2f
            lineDataSet.setDrawCircleHole(false)
            lineDataSet.setDrawCircles(false)
            lineDataSet.setDrawValues(false)
            lineDataSet.setDrawFilled(true)
            lineDataSet.formSize = 0f
            lineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER

            lineDataSet.fillDrawable = ContextCompat.getDrawable(context, R.drawable.gradient_chart)

            val dataSets = listOf(lineDataSet)
            val data = LineData(dataSets)

            // set data
            todayStatsChart.data = data
        }
    }

    private fun setupChartStyle() {
        todayStatsChart.description.isEnabled = false

        todayStatsChart.setTouchEnabled(false)

        todayStatsChart.isDragEnabled = false
        todayStatsChart.setScaleEnabled(false)

        todayStatsChart.setPinchZoom(false)

        todayStatsChart.axisLeft.setDrawGridLines(false)
        todayStatsChart.xAxis.setDrawGridLines(false)
        todayStatsChart.xAxis.setDrawAxisLine(false)
        todayStatsChart.xAxis.setDrawLabels(false)
        todayStatsChart.axisRight.isEnabled = false
        todayStatsChart.axisLeft.isEnabled = false
        todayStatsChart.setViewPortOffsets(0f, 0f, 0f, 0f)

        todayStatsChart.setDrawBorders(false)
        todayStatsChart.setDrawGridBackground(false)
        todayStatsChart.setDrawMarkers(false)
        todayStatsChart.isHorizontalFadingEdgeEnabled = true
    }
}
