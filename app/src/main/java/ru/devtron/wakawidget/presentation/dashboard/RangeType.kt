package ru.devtron.wakawidget.presentation.dashboard

enum class RangeType(private val jsonName: String) {
    RANGE_7_DAYS("last_7_days"),
    RANGE_30_DAYS("last_30_days");

    override fun toString() = jsonName
}
