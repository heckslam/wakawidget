package ru.devtron.wakawidget.presentation.about

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_about.*
import ru.devtron.wakawidget.BuildConfig
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.framework.PowerSaverHelper
import ru.devtron.wakawidget.framework.PowerSaverHelper.enableAutoStartup
import ru.devtron.wakawidget.framework.PowerSaverHelper.isWhiteListedFromBatteryOptimizations
import ru.devtron.wakawidget.framework.extensions.alert
import ru.devtron.wakawidget.presentation.common.BaseFragment

@AndroidEntryPoint
class AboutAppFragment : BaseFragment(R.layout.fragment_about) {
    override val viewModel: AboutAppViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rate.setOnClickListener {
            onRateClicked()
        }
        version.text = getString(R.string.app_version, BuildConfig.VERSION_NAME)

        offer.setOnClickListener { openLink(getString(R.string.public_offer_uri)) }

        haveTroublesBtn.setOnClickListener {
            if (!isWhiteListedFromBatteryOptimizations(requireContext())) {
                alert(
                    R.string.disable_power_saving,
                    R.string.disable_power_saving_desc,
                    {
                        PowerSaverHelper.getWhiteListingOfBatteryIntent(requireContext())?.let {
                            startActivity(it)
                        }

                        showAddToAutoStartDialog()
                    },
                    R.string.disable
                )
            } else {
                showAddToAutoStartDialog()
            }

        }
    }


    private fun onRateClicked() {
        val uri = Uri.parse("market://details?id=${requireContext().packageName}")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        intent.addFlags(
            Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        )

        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            openLink("http://play.google.com/store/apps/details?id=${requireContext().packageName}")
        }
    }

    private fun openLink(link: String) {
        startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse(link)
            )
        )
    }


}

fun Fragment.showAddToAutoStartDialog() {
    enableAutoStartup(requireContext())?.let {
        alert(
            R.string.allow_app_autostart_title,
            R.string.allow_app_autostart_desc,
            {
                try {
                    startActivity(it)
                } catch (e: Throwable) {
                    Toast.makeText(requireContext(), R.string.done, Toast.LENGTH_SHORT).show()
                }
            }
        )
    }
}

