package ru.devtron.wakawidget.presentation.dashboard.recyclermodels

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import kotlinx.android.synthetic.main.item_card_stats.view.*
import ru.devtron.wakawidget.R

@ModelView(defaultLayout = R.layout.item_card_stats, fullSpan = false)
class ChartColoredItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    @TextProp
    fun setHeader(text: CharSequence) {
        textItem.isSelected = true
        textItem.text = text
    }

    @ModelProp
    fun setLabelColor(color: Int) {
        val labelDrawable: Drawable? = ContextCompat.getDrawable(context, R.drawable.stats_item_label)
        labelDrawable?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        labelColor.background = labelDrawable
    }
}