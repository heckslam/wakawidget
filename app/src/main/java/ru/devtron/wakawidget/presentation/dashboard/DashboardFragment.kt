package ru.devtron.wakawidget.presentation.dashboard

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import com.airbnb.epoxy.EpoxyController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_dashboard.*
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.data.jobs.StatsWorker.Companion.updateWakaWidget
import ru.devtron.wakawidget.domain.dashboard.models.StatsModel
import ru.devtron.wakawidget.framework.extensions.toHumanReadableTime
import ru.devtron.wakawidget.framework.extensions.visible
import ru.devtron.wakawidget.framework.onEmpty
import ru.devtron.wakawidget.framework.onError
import ru.devtron.wakawidget.framework.onStopLoading
import ru.devtron.wakawidget.framework.onSuccess
import ru.devtron.wakawidget.presentation.about.showAddToAutoStartDialog
import ru.devtron.wakawidget.presentation.common.BaseFragment
import ru.devtron.wakawidget.presentation.dashboard.recyclermodels.*
import ru.devtron.wakawidget.presentation.settings.SettingsFragment

@AndroidEntryPoint
class DashboardFragment : BaseFragment(R.layout.fragment_dashboard) {
    override val viewModel: DashboardViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dataState.subscribe {
            recyclerView.withModels {
                it.data.onSuccess { statsModel ->
                    displayStats(this, statsModel)
                    updateWakaWidget(requireContext().applicationContext)
                }.onError {
                    dashErrorOrEmptyStateItem {
                        id(100)
                        clickListener { viewModel.reload() }
                    }
                }.onEmpty {
                    dashErrorOrEmptyStateItem {
                        id(100)
                        titleText(R.string.empty_state_title)
                        description(R.string.empty_state_text)
                        buttonText(R.string.empty_state_action)
                        clickListener {
                            startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse(URL_PLUGINS)
                                )
                            )
                        }
                    }
                }.onStopLoading { progressView?.visible(false) }

                it.leaders.onSuccess {
                    it.forEach {
                        dashLeadersItem {
                            id(it.name.hashCode())
                            header(it.name)
                            leaderBoardData(it.leaders)
                        }
                    }
                }
            }
            if (swipeLayout.isRefreshing) swipeLayout.isRefreshing = false
        }

        viewModel.showAddToAutoStartupDialog.subscribe {
            showAddToAutoStartDialog()
        }

        settings.setOnClickListener {
            SettingsFragment().show(childFragmentManager, SETTINGS_DIALOG_TAG)
        }

        logoutBtn.setOnClickListener {
            showLogoutDialog()
        }
        info.setOnClickListener {
            navController.navigate(R.id.action_dashboardFragment_to_aboutAppFragment)
        }

        swipeLayout.setOnRefreshListener { viewModel.reload() }
    }

    private fun showLogoutDialog() {
        context?.let {
            AlertDialog.Builder(it)
                .setMessage(R.string.logout_message)
                .setPositiveButton(android.R.string.yes) { _, _ -> logout() }
                .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                .show()
        }
    }

    private fun logout() {
        viewModel.logout()
        navController.navigate(R.id.action_dashboardFragment_to_splashFragment)
    }

    private fun displayStats(epoxyController: EpoxyController, statsModel: StatsModel) {
        if (recyclerView == null) return
        val context = recyclerView.context
        with(epoxyController) {
            dashTodayStatsItem {
                id(0)
                todayStats(statsModel.todayDuration.toHumanReadableTime(context))
                percent(statsModel.improvementPercent)
                chartData(statsModel.durations)
                leaderBoardRank(statsModel.rank)
            }

            dashAverageWeekItem {
                id(1)
                totalHours(statsModel.totalSec.toHumanReadableTime(context))
                dailyAverage(statsModel.dailyAverage.toHumanReadableTime(context))
            }
            statsModel.bestDay?.let {
                dashBestDayItem {
                    id(2)
                    date(it.date)
                    time(it.time.toHumanReadableTime(context))
                }
            }

            statsModel.projects?.let {
                dashFlexStatsItem {
                    id(3)
                    header(R.string.stats_card_header_projects)
                    chart(it)
                }
            }

            statsModel.languages?.let {
                dashFlexStatsItem {
                    id(5)
                    header(R.string.stats_card_header_languages)
                    chart(it)
                }
            }

            statsModel.editors?.let {
                dashFlexStatsItem {
                    id(4)
                    header(R.string.stats_card_header_editors)
                    chart(it)
                }
            }

            statsModel.operatingSystems?.let {
                dashFlexStatsItem {
                    id(6)
                    header(R.string.stats_card_header_operating_systems)
                    chart(it)
                }
            }
        }
    }
}

const val URL_PLUGINS = "https://wakatime.com/editors"
const val SETTINGS_DIALOG_TAG = "tag"
