package ru.devtron.wakawidget.presentation.auth

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.CookieManager
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_auth.*
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.framework.extensions.isNetworkAvailable
import ru.devtron.wakawidget.framework.extensions.visible
import ru.devtron.wakawidget.framework.onEmpty
import ru.devtron.wakawidget.framework.onLoading
import ru.devtron.wakawidget.framework.onSuccess
import ru.devtron.wakawidget.presentation.common.BaseFragment

@AndroidEntryPoint
class AuthFragment : BaseFragment(R.layout.fragment_auth) {
    override val viewModel: AuthViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initWebView()

        viewModel.oauthUrlEvent.subscribe { url ->
            url.onSuccess { reloadUrl(it) }
                    .onLoading { progressView.visible(true) }
                    .onEmpty { progressView.visible(false) }
        }
        viewModel.goToDashboardEvent.subscribe {
            navController.navigate(R.id.action_authFragment_to_dashboardFragment)
        }

    }

    private fun reloadUrl(it: String) {
        try {
            val isNetworkAvailable = requireActivity().isNetworkAvailable()
            webView.visible(isNetworkAvailable)
            errorMsg.visible(!isNetworkAvailable)

            if (isNetworkAvailable) {
                webView.loadUrl(it)
            }
        } catch (e: UnsupportedOperationException) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        CookieManager.getInstance().removeAllCookies(null)
        webView.settings.apply {
            javaScriptEnabled = true
        }

        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressView.visible(true)
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                progressView?.visible(false)
            }

            @Suppress("OverridingDeprecatedMember")
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return overrideUrlLoading(url)
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                return overrideUrlLoading(request.url.toString())
            }

            private fun overrideUrlLoading(url: String): Boolean {
                return viewModel.onRedirect(url)
            }
        }
    }
}
