package ru.devtron.wakawidget.presentation.common

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import ru.devtron.wakawidget.framework.arch.BaseViewModel
import ru.devtron.wakawidget.framework.extensions.OnLiveDataAction
import ru.devtron.wakawidget.framework.extensions.hideKeyboard
import ru.devtron.wakawidget.framework.extensions.observe

abstract class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId) {
    abstract val viewModel: BaseViewModel
    val navController by lazy(LazyThreadSafetyMode.NONE) { Navigation.findNavController(requireView()) }

    override fun onStop() {
        super.onStop()
        view?.let {
            activity?.hideKeyboard(it)
        }
    }

    fun <T> LiveData<T>.subscribe(action: OnLiveDataAction<T>): Observer<T> =
        observe(viewLifecycleOwner, action)

    fun <T> BaseViewModel.State<T>.subscribe(action: OnLiveDataAction<T>): Observer<T> =
        observe(viewLifecycleOwner, action)

    fun <T> BaseViewModel.Event<T>.subscribe(action: OnLiveDataAction<T>): Observer<T> =
        observe(viewLifecycleOwner, action)
}
