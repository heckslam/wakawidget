package ru.devtron.wakawidget.presentation.dashboard.recyclermodels

import android.content.Context
import android.util.AttributeSet
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.item_leader.view.*
import kotlinx.android.synthetic.main.item_private_leaders.view.*
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.domain.dashboard.models.Leader
import ru.devtron.wakawidget.framework.extensions.inflate
import ru.devtron.wakawidget.framework.extensions.toHumanReadableTime

@ModelView(defaultLayout = R.layout.item_private_leaders)
class DashLeadersItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    @TextProp
    fun setHeader(text: CharSequence) {
        header.text = text.toString().capitalize()
    }

    @ModelProp
    fun setLeaderBoardData(leaders: List<Leader>) {
        leadersLl.removeAllViews()
        leaders.take(3).forEach {
            val leaderView = leadersLl.inflate(R.layout.item_leader)
            if (it.rank != 1) {
                leaderView.rankTv.text = it.rank.toString()
                leaderView.rankTv.setBackgroundResource(R.drawable.counter_circle)
            }

            leaderView.nameLeaderTv.text = it.userName
            leaderView.statsLeaderTv.text = it.runningTotal.toHumanReadableTime(context)
            leadersLl.addView(leaderView)
        }
    }
}