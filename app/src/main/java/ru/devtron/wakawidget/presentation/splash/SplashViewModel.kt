package ru.devtron.wakawidget.presentation.splash

import dagger.hilt.android.lifecycle.HiltViewModel
import ru.devtron.wakawidget.domain.auth.AuthInteractor
import ru.devtron.wakawidget.framework.arch.BaseViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    authInteractor: AuthInteractor
) : BaseViewModel() {
    val goToDashboardEvent: Event<Unit> = Event()
    val goToAuthEvent: Event<Unit> = Event()

    init {
        if (authInteractor.isSignedIn()) {
            goToDashboardEvent.call()
        } else {
            goToAuthEvent.call()
        }
    }
}
