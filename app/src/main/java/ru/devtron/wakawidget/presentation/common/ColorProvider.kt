package ru.devtron.wakawidget.presentation.common

import android.content.Context
import android.graphics.Color
import ru.devtron.wakawidget.domain.dashboard.models.StatsItem
import java.util.*
import ru.devtron.wakawidget.R
import javax.inject.Inject

class ColorProvider @Inject constructor(private val context: Context) {
    fun provideColors(items: List<StatsItem>): ArrayList<Int> {
        val predefinedColors = context.resources.getIntArray(R.array.chart_colors)
        val colors = ArrayList<Int>(items.size)
        items.forEachIndexed { i, _ ->
            val color: Int = if (i <= predefinedColors.size - 1) {
                predefinedColors[i]
            } else {
                Color.BLACK
            }
            colors.add(color)
        }
        return colors
    }
}