package ru.devtron.wakawidget.presentation.settings.recyclermodels

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.annotation.Nullable
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.airbnb.epoxy.TextProp
import kotlinx.android.synthetic.main.item_switch.view.*
import ru.devtron.wakawidget.R

@ModelView(defaultLayout = R.layout.item_switch)
class SettingsSwitchItem @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    @TextProp
    fun setTitle(text: CharSequence) {
        title.text = text
    }

    @ModelProp
    fun setIcon(@DrawableRes drawableRes: Int) {
        image.setImageResource(drawableRes)
    }

    @ModelProp
    fun setInitialEnabled(enabled: Boolean) {
        item_switch.isChecked = enabled
    }

    @CallbackProp
    fun clickListener(@Nullable onClickListener: ((Boolean) -> Unit)?) {
        onClickListener?.let {
            this.setOnClickListener {
                item_switch.isChecked = !item_switch.isChecked
                it(item_switch.isChecked)
            }
        }
    }
}