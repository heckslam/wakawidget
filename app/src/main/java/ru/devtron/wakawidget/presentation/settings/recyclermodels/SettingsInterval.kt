package ru.devtron.wakawidget.presentation.settings.recyclermodels

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.Nullable
import com.airbnb.epoxy.CallbackProp
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.google.android.material.card.MaterialCardView
import kotlinx.android.synthetic.main.settings_item_interval.view.*
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.framework.extensions.doOnChanged

@ModelView(defaultLayout = R.layout.settings_item_interval)
class SettingsInterval @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialCardView(context, attrs, defStyleAttr) {

    private val minutes = arrayOf(15, 30, 45, 60, 120, 240, 360, 720, 1440, 2880)
    var currentProgress: Int = 0

    @ModelProp
    fun setInitialProgress(initialProgress: Int) {
        currentProgress = minutes.indexOfFirst { it == initialProgress }
        seekBar.progress = currentProgress
        progressTv.text = getTimeString(context)
    }

    @CallbackProp
    fun changeListener(@Nullable onClickListener: ((Int) -> Unit)?) {
        onClickListener?.let {
            seekBar.doOnChanged { _, seekbar_progress, _ ->
                currentProgress = seekbar_progress
                onClickListener.invoke(minutes[currentProgress])
                progressTv.text = getTimeString(context)
            }
        }
    }

    private fun getTimeString(context: Context): String = minutes[currentProgress].let {
        when {
            it < 60 -> context.resources.getQuantityString(R.plurals.stats_time_format_minutes, it, it)
            else -> context.resources.getQuantityString(R.plurals.stats_time_format_hours, it / 60, it / 60)
        }
    }
}