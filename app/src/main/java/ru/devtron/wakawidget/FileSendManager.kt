package ru.devtron.wakawidget

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.hardware.SensorManager
import android.os.Bundle

class FileSendManager private constructor(
    application: Application,
    private val fileManager: FileManager
) : ShakeDetector.OnShakeListener, Application.ActivityLifecycleCallbacks {

    private val shakeDetector: ShakeDetector =
        ShakeDetector(this)
    private var sensorManager: SensorManager =
        application.getSystemService(Context.SENSOR_SERVICE) as SensorManager

    private var activity: Activity? = null

    init {
        application.registerActivityLifecycleCallbacks(this)
    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: FileSendManager? = null

        @JvmStatic
        fun init(
            application: Application,
            fileManager: FileManager
        ) {
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: FileSendManager(
                        application,
                        fileManager
                    ).also { INSTANCE = it }
            }
        }
    }

    override fun hearShake() {
        activity?.let {
            sendEmail(it)
        }
    }

    private fun sendEmail(activity: Activity) {
        val files = arrayOf(fileManager.getLogFile())
        sendFilesByEmail(activity, files)
    }


    override fun onActivityStarted(activity: Activity?) {
        this.activity = activity
        shakeDetector.start(sensorManager)
    }

    override fun onActivityStopped(activity: Activity?) {
        if (this.activity == activity
            && this.activity?.localClassName == activity?.localClassName
        ) {
            shakeDetector.stop()
            this.activity = null
        }
    }

    @Suppress("EmptyFunctionBlock")
    override fun onActivityDestroyed(activity: Activity?) {
    }

    @Suppress("EmptyFunctionBlock")
    override fun onActivityPaused(activity: Activity?) {
    }

    @Suppress("EmptyFunctionBlock")
    override fun onActivityResumed(activity: Activity?) {
    }

    @Suppress("EmptyFunctionBlock")
    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    @Suppress("EmptyFunctionBlock")
    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
    }
}
