package ru.devtron.wakawidget.framework.arch

import androidx.annotation.MainThread
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {
    @MainThread
    fun <T> State<T>.setValueNow(newValue: T) = internalSetValueNow(newValue)
    fun <T> State<T>.setValue(newValue: T) = internalPostValue(newValue)

    @MainThread
    fun <T> Event<T>.callNow(newValue: T) = internalCall(newValue)
    fun <T> Event<T>.call(newValue: T) = internalPostCall(newValue)
    fun Event<Unit>.call() = internalPostCall(Unit)

    class State<T>(initValue: T? = null) {

        private val _liveData = MutableLiveData<T>()

        init {
            if (initValue != null) _liveData.value = initValue
        }

        val liveData: LiveData<T> get() = _liveData

        val value: T? get() = _liveData.value
        val valueNonNull: T get() = value!!
        val hasValue: Boolean get() = value != null

        @MainThread
        internal fun internalSetValueNow(newValue: T) {
            _liveData.value = newValue
        }

        internal fun internalPostValue(newValue: T) {
            _liveData.postValue(newValue)
        }

        @VisibleForTesting(otherwise = VisibleForTesting.NONE)
        fun setTestValue(newValue: T) {
            _liveData.value = newValue
        }

    }

    class Event<T> {

        private val _liveData = LiveEvent<T>()

        val liveData: LiveData<T> get() = _liveData
        val value: T? get() = _liveData.value

        @MainThread
        internal fun internalCall(value: T) {
            _liveData.value = value
        }

        internal fun internalPostCall(value: T) {
            _liveData.postValue(value)
        }

    }
}
