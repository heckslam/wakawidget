package ru.devtron.wakawidget.framework.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.annotation.LayoutRes


fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}


fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

/**
 * Add an action which will be invoked when the SeekBar has the progress changed.
 *
 * @return the [SeekBar.OnSeekBarChangeListener] added to the SeekBar
 * @see SeekBar.OnSeekBarChangeListener.onProgressChanged
 */
fun SeekBar.doOnChanged(action: (seekBar: SeekBar, progress: Int, fromUser: Boolean) -> Unit):
        SeekBar.OnSeekBarChangeListener = setOnSeekBarChangeListener(onChanged = action)

/**
 * Set the listener to this SeekBar using the provided actions.
 */
fun SeekBar.setOnSeekBarChangeListener(
    onStartTracking: ((seekBar: SeekBar) -> Unit)? = null,
    onStopTracking: ((seekBar: SeekBar) -> Unit)? = null,
    onChanged: ((seekBar: SeekBar, progress: Int, fromUser: Boolean) -> Unit)? = null
): SeekBar.OnSeekBarChangeListener {
    val listener = object : SeekBar.OnSeekBarChangeListener {
        override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
            onChanged?.invoke(seekBar, progress, fromUser)
        }

        override fun onStartTrackingTouch(seekBar: SeekBar) {
            onStartTracking?.invoke(seekBar)
        }

        override fun onStopTrackingTouch(seekBar: SeekBar) {
            onStopTracking?.invoke(seekBar)
        }
    }
    setOnSeekBarChangeListener(listener)
    return listener
}
