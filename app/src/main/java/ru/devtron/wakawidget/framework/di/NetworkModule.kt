package ru.devtron.wakawidget.framework.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.devtron.wakawidget.data.server.WakatimeApi
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule : BaseServiceModule<WakatimeApi>() {
    @Singleton
    @Provides
    fun provideService(@ApplicationContext context: Context) =
        provideService(context, WakatimeApi::class)
}
