package ru.devtron.wakawidget.framework.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.devtron.wakawidget.data.local.AppDatabase
import ru.devtron.wakawidget.data.server.WakatimeApi
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule : BaseServiceModule<WakatimeApi>() {
    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) =
        buildDatabase(context)
}

fun buildDatabase(context: Context) =
    Room.databaseBuilder(
        context.applicationContext,
        AppDatabase::class.java, "next"
    ).apply {
        fallbackToDestructiveMigration()
        allowMainThreadQueries() //todo
    }.build()
