package ru.devtron.wakawidget.framework

import android.annotation.TargetApi
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.PowerManager
import android.provider.Settings


object PowerSaverHelper {

    fun isWhiteListedFromBatteryOptimizations(
        context: Context
    ): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true
        val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager?
            ?: return false
        return pm.isIgnoringBatteryOptimizations(context.packageName)
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun getWhiteListingOfBatteryIntent(
        context: Context
    ): Intent? {
        val appIsWhiteListedFromPowerSave = isWhiteListedFromBatteryOptimizations(context)
        return if (!appIsWhiteListedFromPowerSave) Intent(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS) else null
    }


    fun enableAutoStartup(context: Context): Intent? {
        val intent = Intent()
        try {

            val manufacturer = Build.MANUFACTURER
            when {
                "xiaomi".equals(manufacturer, ignoreCase = true) -> intent.component = ComponentName(
                    "com.miui.securitycenter",
                    "com.miui.permcenter.autostart.AutoStartManagementActivity"
                )
                "oppo".equals(manufacturer, ignoreCase = true) -> intent.component = ComponentName(
                    "com.coloros.safecenter",
                    "com.coloros.safecenter.permission.startup.StartupAppListActivity"
                )
                "vivo".equals(manufacturer, ignoreCase = true) -> intent.component = ComponentName(
                    "com.vivo.permissionmanager",
                    "com.vivo.permissionmanager.activity.BgStartUpManagerActivity"
                )
                "Letv".equals(manufacturer, ignoreCase = true) -> intent.component =
                    ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")
                "Honor".equals(manufacturer, ignoreCase = true) -> intent.component = ComponentName(
                    "com.huawei.systemmanager",
                    "com.huawei.systemmanager.optimize.process.ProtectActivity"
                )
            }

            val list = context.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)

            return if (list.size > 0) intent else null
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }
}