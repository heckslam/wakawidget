package ru.devtron.wakawidget.framework

import ru.devtron.wakawidget.BuildConfig
import ru.devtron.wakawidget.FileManager.saveLogToInternal
import timber.log.Timber

sealed class LogLevel
object DEBUG : LogLevel()
object INFO : LogLevel()
object WARN : LogLevel()
object ERROR : LogLevel()

fun Any.log(lazyMessage: Any, level: LogLevel = DEBUG) {
    val message = "${this::class.java.simpleName}, $lazyMessage"
    saveLogToInternal(message)
    if (BuildConfig.DEBUG) {
        when (level) {
            DEBUG -> Timber.d(message)
            INFO -> Timber.i(message)
            WARN -> Timber.w(message)
            ERROR -> Timber.e(message)
        }
    }
}

fun Any.logAndSaveToFile(lazyMessage: Any) {
    log(lazyMessage)
    val message = "${this::class.java.simpleName}, $lazyMessage"
    saveLogToInternal(message)
}
