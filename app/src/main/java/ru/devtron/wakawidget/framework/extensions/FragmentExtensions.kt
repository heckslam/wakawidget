package ru.devtron.wakawidget.framework.extensions

import androidx.annotation.DimenRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import ru.devtron.wakawidget.R


fun Fragment.alert(
    title: Int,
    desc: Int,
    yesListener: () -> Unit,
    yesBtnText: Int = R.string.ok,
    noListener: (() -> Unit)? = null
) = AlertDialog.Builder(requireContext(), R.style.PauseDialog).apply {
    setTitle(title)
    setMessage(desc)
    setPositiveButton(yesBtnText) { dialog, _ ->
        yesListener()
        dialog.cancel()
    }
    setNegativeButton(R.string.cancel) { dialog, _ ->
        noListener?.invoke()
        dialog.cancel()
    }
    show()
}

fun Fragment.fromDimen(@DimenRes dimenRes: Int) = resources.getDimensionPixelSize(dimenRes)
