package ru.devtron.wakawidget.framework.api

import com.squareup.moshi.Moshi
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.threeten.bp.LocalDate
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.devtron.wakawidget.BuildConfig
import ru.devtron.wakawidget.data.server.adapter.LocalDateAdapter
import ru.devtron.wakawidget.data.server.interceptor.AuthHeaderInterceptor
import java.util.concurrent.TimeUnit


class RetrofitBuilder {

    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.ORIGIN_ENDPOINT)
            .addConverterFactory(MoshiConverterFactory.create(
                Moshi.Builder().add(LocalDate::class.java, LocalDateAdapter())
                    .build()))
            .client(okHttpClient)
            .build()
    }

    fun provideOkHttpClient(cache: Cache): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(AuthHeaderInterceptor())
            .apply {
                applyCommonConfigForOkhttp(this, cache)
            }
            .build()
    }

    private fun applyCommonConfigForOkhttp(
        builder: OkHttpClient.Builder,
        cache: Cache
    ) {
        builder.cache(cache)
            .connectTimeout(OKHTTP_CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(OKHTTP_WRITE_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(OKHTTP_READ_TIMEOUT, TimeUnit.SECONDS)
            .apply {
                if (BuildConfig.DEBUG) {
                    val logging = HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    }
                    addInterceptor(logging)
                }
            }
    }

    companion object {
        const val CACHE_SIZE = 10 * 1024 * 1024L // 10 MB
        const val OKHTTP_CONNECT_TIMEOUT = 20L
        const val OKHTTP_WRITE_TIMEOUT = 60L
        const val OKHTTP_READ_TIMEOUT = 60L
    }
}
