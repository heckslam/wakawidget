package ru.devtron.wakawidget.framework.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ru.devtron.wakawidget.BuildConfig
import ru.devtron.wakawidget.domain.auth.OAuthParams
import ru.devtron.wakawidget.framework.ResourceManager
import ru.devtron.wakawidget.presentation.common.ColorProvider
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideOAuthParams(): OAuthParams = OAuthParams(
        BuildConfig.WAKA_CLIENT_ID,
        BuildConfig.WAKA_CLIENT_SECRET,
        BuildConfig.WAKA_REDIRECT_URL,
        BuildConfig.WAKA_SCOPES.joinToString(separator = ",")
    )

    @Singleton
    @Provides
    fun provideResourceManager(@ApplicationContext context: Context): ResourceManager =
        ResourceManager(context)

    @Singleton
    @Provides
    fun provideColorProvider(@ApplicationContext context: Context): ColorProvider =
        ColorProvider(context)
}
