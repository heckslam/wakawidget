package ru.devtron.wakawidget.framework.extensions

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import ru.devtron.wakawidget.framework.arch.BaseViewModel

typealias OnLiveDataAction<T> = (data: T) -> Unit

fun <T> LiveData<T>.observe(owner: LifecycleOwner, action: OnLiveDataAction<T>): Observer<T> {
    val observer = Observer<T> { if (it != null) action(it) }
    observe(owner, observer)
    return observer
}

fun <T> BaseViewModel.Event<T>.observe(
    owner: LifecycleOwner,
    action: OnLiveDataAction<T>
): Observer<T> = liveData.observe(owner, action)

fun <T> BaseViewModel.State<T>.observe(
    owner: LifecycleOwner,
    action: OnLiveDataAction<T>
): Observer<T> = liveData.observe(owner, action)

fun <T> AppCompatActivity.observe(
    state: BaseViewModel.State<T>,
    action: OnLiveDataAction<T>
): Observer<T> = state.observe(this, action)

fun <T> AppCompatActivity.observe(
    event: BaseViewModel.Event<T>,
    action: OnLiveDataAction<T>
): Observer<T> = event.observe(this, action)

fun <T> Fragment.observe(
    state: BaseViewModel.State<T>,
    action: OnLiveDataAction<T>
): Observer<T> = state.observe(this, action)

fun <T> Fragment.observe(
    event: BaseViewModel.Event<T>,
    action: OnLiveDataAction<T>
): Observer<T> = event.observe(this, action)

fun <T> Fragment.observeView(
    state: BaseViewModel.State<T>,
    action: OnLiveDataAction<T>
): Observer<T> = state.observe(viewLifecycleOwner, action)

fun <T> Fragment.observeView(
    event: BaseViewModel.Event<T>,
    action: OnLiveDataAction<T>
): Observer<T> = event.observe(viewLifecycleOwner, action)
