package ru.devtron.wakawidget.framework.arch

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

/**
 * Created on 03/10/2018 by ruslanaliev.
 */
interface BaseDao<Entity> {

    /**
     * Insert an object in the database.
     *
     * @param obj the object to be inserted.
     */
    @Insert
    fun insert(obj: Entity)

    /**
     * Insert an array of objects in the database.
     *
     * @param obj the objects to be inserted.
     */
    @Insert
    fun insert(vararg obj: Entity)

    /**
     * Update an object from the database.
     *
     * @param obj the object to be updated
     */
    @Update
    fun update(obj: Entity)

    /**
     * Delete an object from the database
     *
     * @param obj the object to be deleted
     */
    @Delete
    fun delete(obj: Entity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertIgnore(entity: Entity): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateIgnore(entity: Entity)
}

/**
 * Upsert-like chain, because upsert is not supported by sqlite
 * On faillure to insert it returns -1 as index of the the inserted element
 */
suspend fun <T> BaseDao<T>.upsert(entity: T) {
    val id = insertIgnore(entity)
    if (id == -1L) {
        updateIgnore(entity)
    }
}