package ru.devtron.wakawidget.framework

import android.content.Context
import android.os.Build
import androidx.work.*
import ru.devtron.wakawidget.WakaApplication
import ru.devtron.wakawidget.data.jobs.StatsWorker
import ru.devtron.wakawidget.data.local.User
import ru.devtron.wakawidget.data.local.Worker
import java.util.concurrent.TimeUnit

/**
 * Helps to deal with Work Manager.
 */

const val IDLE_MIN_SDK_VERSION = 23
const val STATS_WORKER_UNIQUE_WORK_NAME = "STATS_UPDATE_WORK"

object WorkerHelper {

    const val WIFI = "wifi"

    fun updateWorkerWithConstraints(
        context: Context = WakaApplication.instance
    ) {
        logAndSaveToFile("WorkHelper: updateWorkerWithConstraints")
        if (User.token.isNotEmpty()) {
            reloadWorkManager(Worker.workerDelay.toLong(), context)
        } else {
            cancelWork()
        }
    }

    private fun reloadWorkManager(delay: Long, context: Context) {
        logAndSaveToFile("WorkHelper: reloadWorkManager with delay $delay")
        val workerConstraints = Constraints.Builder()
        workerConstraints.setRequiredNetworkType(NetworkType.CONNECTED)

        val inputData = Data.Builder()
            .putBoolean(WIFI, Worker.wifiOnly)
            .build()

        workerConstraints.setRequiresBatteryNotLow(Worker.batteryNotLow)
        workerConstraints.setRequiresCharging(Worker.charginOnly)
        if (Build.VERSION.SDK_INT >= IDLE_MIN_SDK_VERSION && Worker.deviceIdle) {
            workerConstraints.setRequiresDeviceIdle(true)
        }

        val syncWork = PeriodicWorkRequestBuilder<StatsWorker>(delay, TimeUnit.MINUTES)
            .setInitialDelay(delay, TimeUnit.MINUTES)
            .setConstraints(workerConstraints.build())
            .setInputData(inputData)
            .build()

        WorkManager.getInstance(context)
            .enqueueUniquePeriodicWork(STATS_WORKER_UNIQUE_WORK_NAME, ExistingPeriodicWorkPolicy.KEEP, syncWork)
    }

    fun cancelWork() {
        logAndSaveToFile("WorkHelper: cancelWork")
        WorkManager.getInstance(WakaApplication.instance).cancelUniqueWork(STATS_WORKER_UNIQUE_WORK_NAME)
    }
}
