package ru.devtron.wakawidget.framework.extensions

import android.content.Context
import ru.devtron.wakawidget.R
import java.util.concurrent.TimeUnit


fun Int.toHumanReadableTime(context: Context): String {
    val hours = TimeUnit.SECONDS.toHours(this.toLong())
    val minutes = TimeUnit.SECONDS.toMinutes(this.toLong()) - TimeUnit.HOURS.toMinutes(hours)
    val templateHours = context.resources.getQuantityString(
        R.plurals.stats_time_format_hours,
        hours.toInt(),
        hours.toInt()
    )
    val templateMinutes = context.resources.getQuantityString(
        R.plurals.stats_time_format_minutes,
        minutes.toInt(),
        minutes.toInt()
    )
    val timeBuilder = StringBuilder()
    timeBuilder.append(templateHours.format(hours)).append(' ')
    timeBuilder.append(templateMinutes.format(minutes))
    return timeBuilder.toString()
}

fun Int.toHumanReadableTimeShort(context: Context): String {
    val hours = TimeUnit.SECONDS.toHours(this.toLong())
    val minutes = TimeUnit.SECONDS.toMinutes(this.toLong()) - TimeUnit.HOURS.toMinutes(hours)
    return context.getString(R.string.widget_time, hours.toInt(), minutes.toInt())
}
