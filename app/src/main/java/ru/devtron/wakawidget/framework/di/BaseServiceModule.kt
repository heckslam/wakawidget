package ru.devtron.wakawidget.framework.di

import android.content.Context
import okhttp3.Cache
import ru.devtron.wakawidget.framework.api.RetrofitBuilder
import kotlin.reflect.KClass


open class BaseServiceModule<S : Any> {

    private fun provideOkhttpCache(context: Context): Cache {
        val cacheDir = context.cacheDir ?: context.filesDir
        return Cache(cacheDir, RetrofitBuilder.CACHE_SIZE)
    }

    protected fun provideService(
        context: Context,
        serviceClass: KClass<S>
    ): S = with(RetrofitBuilder()) {
        provideRetrofit(
            provideOkHttpClient(
                provideOkhttpCache(context)
            )
        ).create(serviceClass.java)
    }

}
