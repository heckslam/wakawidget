package ru.devtron.wakawidget.framework

import android.content.Context

class ResourceManager(val context: Context) {

    fun getString(id: Int) = context.getString(id)
    fun getQuanityString(id: Int, param: Int) =
        context.resources.getQuantityString(id, param, param)
}
