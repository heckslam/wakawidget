package ru.devtron.wakawidget

import android.annotation.SuppressLint
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import ru.devtron.wakawidget.framework.log
import timber.log.Timber
import java.io.File
import java.io.FileWriter
import java.io.IOException

object FileManager {
    private const val DIRECTORY_LOG_SERVER = "log"
    private const val DATE_FORMAT = "yyyy-MM-dd"
    private const val LOG_DATE_FORMAT = "YY.MM.dd-HH:mm:ss"
    private val FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT)
    private val FORMATTER_LOG = DateTimeFormatter.ofPattern(LOG_DATE_FORMAT)

    private val logFileName = "a_log_" +
            FORMATTER.format(LocalDate.now()) + ".txt"

    fun saveLogToInternal(
        message: String
    ) {
        if (!BuildConfig.DEBUG) {
            return
        }
        val reqDir = File(getInternalFile(), DIRECTORY_LOG_SERVER)
        val isCreated = reqDir.exists() && reqDir.isDirectory || reqDir.mkdir()
        if (isCreated) {
            val reqFile = File(reqDir, logFileName)
            try {
                FileWriter(reqFile, true).use { fileWriter ->
                    fileWriter.write(getTextForFile("LogMessage", message))
                }
            } catch (@SuppressLint("NoLoggedException") e: IOException) {
                e.printStackTrace()
                Timber.e(e)
                log("e $e")
            }
        }
    }

    private fun getTextForFile(name: String, body: Any): String? {
        return String.format(
            "%s: %s: %s\n",
            FORMATTER_LOG.format(LocalDateTime.now()), name, body
        )
    }

    private fun getInternalFile(): File {
        return WakaApplication.instance.applicationContext.filesDir
    }

    fun getLogFile(): File? {
        val reqDir = File(getInternalFile(), DIRECTORY_LOG_SERVER)
        return File(reqDir, logFileName)
    }

}
