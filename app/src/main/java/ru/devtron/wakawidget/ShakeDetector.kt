package ru.devtron.wakawidget

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager


class ShakeDetector(private val onShakeListener: OnShakeListener) : SensorEventListener {

    companion object {
        const val DEFAULT_ACCELERATION_THRESHOLD = 18
        const val TIMEOUT_OF_SHAKE = 5_000_000_000 // 5sec
    }

    private var hearShakeAt: Long = 0
    private val queue = SampleQueue()

    private var sensorManager: SensorManager? = null
    private var accelerometer: Sensor? = null
    /**
     * When the magnitude of total acceleration exceeds this
     * value, the phone is accelerating.
     */
    private val accelerationThreshold = DEFAULT_ACCELERATION_THRESHOLD

    /**
     * Starts listening for shakes on devices with appropriate hardware.
     *
     * @return true if the device supports shake detection.
     */
    fun start(sensorManager: SensorManager): Boolean {
        // Already started?
        if (accelerometer != null) {
            return true
        }

        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        // If this phone has an accelerometer, listen to it.
        if (accelerometer != null) {
            this.sensorManager = sensorManager
            sensorManager.registerListener(this, accelerometer,
                SensorManager.SENSOR_DELAY_FASTEST)
        }
        return accelerometer != null
    }

    /**
     * Stops listening.  Safe to call when already stopped.  Ignored on devices
     * without appropriate hardware.
     */
    fun stop() {
        if (accelerometer != null) {
            queue.clear()
            sensorManager?.unregisterListener(this, accelerometer)
            sensorManager = null
            accelerometer = null
        }
    }

    override fun onSensorChanged(event: SensorEvent) {
        val accelerating = isAccelerating(event)
        val timestamp = event.timestamp
        queue.add(timestamp, accelerating)
        if (queue.isShaking && hearShakeAt < timestamp - TIMEOUT_OF_SHAKE) {
            queue.clear()
            onShakeListener.hearShake()
            hearShakeAt = timestamp
        }
    }

    /** Returns true if the device is currently accelerating.  */
    private fun isAccelerating(event: SensorEvent): Boolean {
        val ax = event.values[0]
        val ay = event.values[1]
        val az = event.values[2]

        // Instead of comparing magnitude to ACCELERATION_THRESHOLD,
        // compare their squares. This is equivalent and doesn't need the
        // actual magnitude, which would be computed using (expensive) Math.sqrt().
        val magnitudeSquared = (ax * ax + ay * ay + az * az).toDouble()
        return magnitudeSquared > accelerationThreshold * accelerationThreshold
    }

    interface OnShakeListener {
        fun hearShake()
    }


    /** Queue of samples. Keeps a running average.  */
    internal class SampleQueue {

        private val pool = SamplePool()

        private var oldest: Sample? = null
        private var newest: Sample? = null
        private var sampleCount: Int = 0
        private var acceleratingCount: Int = 0

        /**
         * Returns true if we have enough samples and more than 3/4 of those samples
         * are accelerating.
         */
        val isShaking: Boolean
            get() = (newest != null
                    && oldest != null
                    && newest!!.timestamp - oldest!!.timestamp >= MIN_WINDOW_SIZE
                    && acceleratingCount >= (sampleCount shr 1) + (sampleCount shr 2))

        /**
         * Adds a sample.
         *
         * @param timestamp    in nanoseconds of sample
         * @param accelerating true if > [.accelerationThreshold].
         */
        fun add(timestamp: Long, accelerating: Boolean) {
            // Purge samples that proceed window.
            purge(timestamp - MAX_WINDOW_SIZE)

            // Add the sample to the queue.
            val added = pool.acquire()
            added.timestamp = timestamp
            added.accelerating = accelerating
            added.next = null
            newest?.next = added
            newest = added
            if (oldest == null) {
                oldest = added
            }

            // Update running average.
            sampleCount++
            if (accelerating) {
                acceleratingCount++
            }
        }

        /** Removes all samples from this queue.  */
        fun clear() {
            while (oldest != null) {
                val removed = oldest
                oldest = removed!!.next
                pool.release(removed)
            }
            newest = null
            sampleCount = 0
            acceleratingCount = 0
        }

        /** Purges samples with timestamps older than cutoff.  */
        private fun purge(cutoff: Long) {
            while (sampleCount >= MIN_QUEUE_SIZE
                && oldest != null && cutoff - oldest!!.timestamp > 0) {
                // Remove sample.
                val removed = oldest
                if (removed!!.accelerating) {
                    acceleratingCount--
                }
                sampleCount--

                oldest = removed.next
                if (oldest == null) {
                    newest = null
                }
                pool.release(removed)
            }
        }

        companion object {

            /** Window size in ns. Used to compute the average.  */
            private const val MAX_WINDOW_SIZE: Long = 1_000_000_000 // 1s
            private const val MIN_WINDOW_SIZE = MAX_WINDOW_SIZE shr 1 // 0.5s

            /**
             * Ensure the queue size never falls below this size, even if the device
             * fails to deliver this many events during the time window. The LG Ally
             * is one such device.
             */
            private const val MIN_QUEUE_SIZE = 4
        }
    }

    /** An accelerometer sample.  */
    internal class Sample {
        /** Time sample was taken.  */
        var timestamp: Long = 0

        /** If acceleration > [.accelerationThreshold].  */
        var accelerating: Boolean = false

        /** Next sample in the queue or pool.  */
        var next: Sample? = null
    }

    /** Pools samples. Avoids garbage collection.  */
    internal class SamplePool {
        private var head: Sample? = null

        /** Acquires a sample from the pool.  */
        fun acquire(): Sample {
            var acquired = head
            if (acquired == null) {
                acquired = Sample()
            } else {
                // Remove instance from pool.
                head = acquired.next
            }
            return acquired
        }

        /** Returns a sample to the pool.  */
        fun release(sample: Sample) {
            sample.next = head
            head = sample
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        //ignore
    }
}
