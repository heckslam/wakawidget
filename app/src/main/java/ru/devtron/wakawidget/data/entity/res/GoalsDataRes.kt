package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GoalsDataRes(
    @Json(name = "chart_data")
    val chartData: List<ChartDataRes>
)
