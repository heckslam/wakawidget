package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PrivateLeadersBoardsRes(
    @Json(name = "data")
    val board: List<Data>,
    val page: Int,
    @Json(name = "total_pages")
    val totalPages: Int
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        val id: String,
        @Json(name = "members_count")
        val membersCount: Int,
        val name: String,
        @Json(name = "time_range")
        val timeRange: String
    )
}
