package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class PrivateLeadersRes(
    @Json(name = "data")
    val leaders: List<Data>,
    @Json(name = "current_user")
    val currentUser: CurrentUser?
)

@JsonClass(generateAdapter = true)
data class Data(
    val rank: Int,
    @Json(name = "running_total")
    val runningTotal: RunningTotal,
    val user: User
)

@JsonClass(generateAdapter = true)
data class CurrentUser(
    val rank: Int?,
    @Json(name = "running_total")
    val runningTotal: RunningTotal?,
    val user: User
)

@JsonClass(generateAdapter = true)
data class RunningTotal(
    @Json(name = "total_seconds")
    val totalSeconds: Double
)

@JsonClass(generateAdapter = true)
data class User(
    @Json(name = "full_name")
    val fullName: String? = null,
    @Json(name = "display_name")
    val displayName: String? = null
)
