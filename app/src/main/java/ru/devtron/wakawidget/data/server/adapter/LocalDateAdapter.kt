package ru.devtron.wakawidget.data.server.adapter

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

/**
 * Created on 03/10/2018 by ruslanaliev.
 */
class LocalDateAdapter : JsonAdapter<LocalDate>() {
    override fun toJson(writer: JsonWriter, value: LocalDate?) {
        writer.value(FORMATTER.format(value))
    }

    override fun fromJson(reader: JsonReader): LocalDate? {
        return LocalDate.parse(reader.nextString(), FORMATTER)
    }

    companion object {
        private val FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    }
}