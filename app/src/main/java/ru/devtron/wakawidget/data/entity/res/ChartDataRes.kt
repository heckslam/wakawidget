package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class ChartDataRes(
    @Json(name = "actual_seconds")
    val actualSeconds: Float,
    @Json(name = "goal_seconds")
    val goalSeconds: Float
)
