package ru.devtron.wakawidget.data.repos

import com.chibatching.kotpref.bulk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.devtron.wakawidget.data.entity.res.TokenDataRes
import ru.devtron.wakawidget.data.local.User
import ru.devtron.wakawidget.data.server.WakatimeApi
import ru.devtron.wakawidget.framework.WorkerHelper
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepo @Inject constructor(
    private val restApi: WakatimeApi
) {

    val isSignedIn get() = User.token.isNotEmpty()

    suspend fun requestOAuthToken(
        code: String,
        redirectUri: String,
        appId: String,
        appKey: String
    ): TokenDataRes = withContext(Dispatchers.IO) {
        restApi.auth(
            code,
            redirectUri,
            appId,
            appKey
        )
    }

    fun saveAuthData(token: String, userId: String) {
        User.bulk {
            this.token = token
            this.userId = userId
        }
    }

    fun clearAuthData() {
        WorkerHelper.cancelWork()
        User.bulk {
            token = ""
            userId = ""
        }
    }
}
