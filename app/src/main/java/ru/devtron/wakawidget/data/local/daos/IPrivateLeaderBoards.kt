package ru.devtron.wakawidget.data.local.daos

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.devtron.wakawidget.data.entity.db.LeaderBoardsEntity
import ru.devtron.wakawidget.framework.arch.BaseDao

@Dao
interface IPrivateLeaderBoards : BaseDao<LeaderBoardsEntity> {
    @Query("SELECT * FROM leaderboardsentity")
    fun getLeaderBoards(): Flow<LeaderBoardsEntity?>
}
