package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
open class BaseStatsItem {
    @Json(name = "hours")
    var hours: Int = 0
    @Json(name = "minutes")
    var minutes: Int = 0
    @Json(name = "name")
    var name: String = ""
    @Json(name = "percent")
    var percent: Double = 0.0
    @Json(name = "total_seconds")
    var totalSeconds: Float = 0f
}

@JsonClass(generateAdapter = true)
class Project : BaseStatsItem()

@JsonClass(generateAdapter = true)
class OperatingSystem : BaseStatsItem()

@JsonClass(generateAdapter = true)
class Language : BaseStatsItem()

@JsonClass(generateAdapter = true)
class Editor : BaseStatsItem()
