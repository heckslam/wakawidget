package ru.devtron.wakawidget.data.mapper

import org.threeten.bp.format.DateTimeFormatter
import ru.devtron.wakawidget.data.entity.res.*
import ru.devtron.wakawidget.domain.dashboard.models.BestDay
import ru.devtron.wakawidget.domain.dashboard.models.StatsItem
import ru.devtron.wakawidget.domain.dashboard.models.StatsModel
import ru.devtron.wakawidget.presentation.common.ColorProvider
import javax.inject.Inject

class StatsMapper @Inject constructor(private val colorProvider: ColorProvider) {

    fun map(
        publicLeader: PublicLeaderBoardRes,
        statsWrapper: StatsWrapperRes,
        durations: DurationWrapperRes,
        goals: GoalsWrapperRes
    ): StatsModel {
        val stats = statsWrapper.statsRes

        val lastDayGoal = goals.data.firstOrNull()?.chartData?.lastOrNull()
            ?: ChartDataRes(0f, 25200f)
        val improvementPercent = (lastDayGoal.actualSeconds * 100.0) / lastDayGoal.goalSeconds

        return StatsModel(
            stats.bestDay.map(),
            stats.dailyAverage,
            stats.totalSeconds?.toInt() ?: 0,
            stats.projects?.filterAndMap(),
            stats.languages?.filterAndMap(),
            stats.editors?.filterAndMap(),
            stats.operatingSystems?.filterAndMap(),
            stats.range,
            System.currentTimeMillis(),
            stats.totalSeconds?.toInt() == 0,
            durations.data,
            improvementPercent,
            publicLeader.currentUser?.rank
        )
    }
    private fun List<StatsItem>.provideColors(): List<StatsItem> {
        val colors = colorProvider.provideColors(this)
        zip(colors) { item, color -> item.color = color }
        return this
    }

    private fun List<BaseStatsItem>.filterAndMap() = filter { it.percent > 2 }
        .map {
            StatsItem(it.hours, it.minutes, it.name, it.percent, it.totalSeconds.toInt())
        }.provideColors()

    private fun BestDayRes?.map(): BestDay? {
        val date = this?.date
        val timeSec = this?.totalSeconds
        return if (date != null && timeSec != null)
            BestDay(date.format(DateTimeFormatter.ofPattern("dd MMMM YYYY")), timeSec.toInt())
        else null
    }
}
