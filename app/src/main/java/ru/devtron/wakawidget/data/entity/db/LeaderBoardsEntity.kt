package ru.devtron.wakawidget.data.entity.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import ru.devtron.wakawidget.domain.dashboard.models.Leader
import ru.devtron.wakawidget.domain.dashboard.models.LeaderBoards

@Entity
data class LeaderBoardsEntity(
    val leaderBoards: List<LeadersBoardEntity>,
    @PrimaryKey
    val id: Int = 1
)
@JsonClass(generateAdapter = true)
data class LeadersBoardEntity(
    val name: String,
    val leaders: List<Leader>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as LeaderBoards

        if (name != other.name) return false

        return true
    }

    override fun hashCode() = name.hashCode() * 31
}
