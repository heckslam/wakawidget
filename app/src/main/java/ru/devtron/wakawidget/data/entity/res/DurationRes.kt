package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DurationRes(val duration: Double)
