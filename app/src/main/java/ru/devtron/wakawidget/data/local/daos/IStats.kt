package ru.devtron.wakawidget.data.local.daos

import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import ru.devtron.wakawidget.data.entity.db.StatsEntity
import ru.devtron.wakawidget.framework.arch.BaseDao

@Dao
interface IStats : BaseDao<StatsEntity> {
    @Query("SELECT * FROM statsentity")
    fun getStatsFlow(): Flow<StatsEntity?>
    @Query("SELECT * FROM statsentity")
    fun getStats(): StatsEntity?
}
