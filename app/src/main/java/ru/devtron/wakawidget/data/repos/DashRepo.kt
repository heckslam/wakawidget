package ru.devtron.wakawidget.data.repos

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import ru.devtron.wakawidget.data.entity.db.LeaderBoardsEntity
import ru.devtron.wakawidget.data.entity.db.StatsEntity
import ru.devtron.wakawidget.data.entity.res.DurationWrapperRes
import ru.devtron.wakawidget.data.entity.res.GoalsWrapperRes
import ru.devtron.wakawidget.data.entity.res.PublicLeaderBoardRes
import ru.devtron.wakawidget.data.entity.res.StatsWrapperRes
import ru.devtron.wakawidget.data.local.AppDatabase
import ru.devtron.wakawidget.data.local.daos.IPrivateLeaderBoards
import ru.devtron.wakawidget.data.local.daos.IStats
import ru.devtron.wakawidget.data.mapper.StatsMapper
import ru.devtron.wakawidget.data.mapper.toLeaderboardsModelEntity
import ru.devtron.wakawidget.data.mapper.toStatsModelEntity
import ru.devtron.wakawidget.data.server.WakatimeApi
import ru.devtron.wakawidget.domain.dashboard.models.Leader
import ru.devtron.wakawidget.domain.dashboard.models.LeaderBoards
import ru.devtron.wakawidget.domain.dashboard.models.StatsModel
import ru.devtron.wakawidget.framework.arch.upsert
import ru.devtron.wakawidget.framework.logAndSaveToFile
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DashRepo @Inject constructor(
    db: AppDatabase, private val restApi: WakatimeApi, private val statsMapper: StatsMapper
) {
    private val statsDao: IStats = db.statsDao()
    private val leaderBoardDao: IPrivateLeaderBoards = db.leaderBoardsDao()
    private val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss")

    suspend fun getCurrentUserStats(range: String, date: String) = withContext(Dispatchers.IO) {
        val publicRank = async { restApi.getPublicRank() }
        val currentStats = async { restApi.getCurrentUserStats(range) }
        val durations = async { restApi.getDurations(date) }
        val goals = async { restApi.getGoals() }
        try {
            saveStats(publicRank.await(), currentStats.await(), durations.await(), goals.await())
        } catch (e: Exception) {
            logAndSaveToFile("getCurrentUserStats error ${e.message}")
            e.printStackTrace()
        }
    }

    private suspend fun saveStats(
        publicLeader: PublicLeaderBoardRes,
        statsWrapper: StatsWrapperRes,
        durations: DurationWrapperRes,
        goals: GoalsWrapperRes
    ) {

        val mappedStats = statsMapper.map(publicLeader, statsWrapper, durations, goals)
        try {
            saveStats(mappedStats)
        } catch (e: Exception) {
            logAndSaveToFile("saveStats error ${e.message} ")
            e.printStackTrace()
        }
    }


    suspend fun getPrivateLeaderBoards() = withContext(Dispatchers.IO) {
        val privateLeadersBoardsRes = restApi.getPrivateLeaderBoards()
        val quiedPrivateLeadersBoardsRes = privateLeadersBoardsRes.board.map {
            async { getPrivateLeaders(it.id) }
        }


        val result =
            quiedPrivateLeadersBoardsRes.awaitAll().mapIndexed { index, privateLeadersRes ->
                LeaderBoards(privateLeadersBoardsRes.board[index].name,
                    privateLeadersRes.leaders.map {
                        Leader(
                            it.rank,
                            it.runningTotal.totalSeconds.toInt(),
                            it.user.displayName
                                ?: it.user.fullName.orEmpty()
                        )
                    })
            }

        saveLeaders(result)
    }

    private suspend fun getPrivateLeaders(boardId: String) = withContext(Dispatchers.IO) {
        restApi.getPrivateLeaders(boardId)
    }

    private suspend fun saveStats(stats: StatsModel) = withContext(Dispatchers.IO) {
        statsDao.upsert(
            StatsEntity(
                stats.toStatsModelEntity(), dateTimeFormatter.format(
                    LocalDateTime.now()
                )
            )
        )
    }

    private suspend fun saveLeaders(leaders: List<LeaderBoards>) = withContext(Dispatchers.IO) {
        leaderBoardDao.upsert(leaders.toLeaderboardsModelEntity())
    }

    fun getCachedStats(): Flow<StatsEntity?> {
        return statsDao.getStatsFlow().flowOn(Dispatchers.IO)
    }

    fun getCachedLeads(): Flow<LeaderBoardsEntity?> {
        return leaderBoardDao.getLeaderBoards().flowOn(Dispatchers.IO)
    }
}
