package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.threeten.bp.LocalDate

@JsonClass(generateAdapter = true)
data class BestDayRes(
    val date: LocalDate?,
    @Json(name = "total_seconds")
    val totalSeconds: Float?
)
