package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DurationWrapperRes(
    val data: List<DurationRes>
)
