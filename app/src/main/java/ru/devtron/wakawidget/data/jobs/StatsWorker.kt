package ru.devtron.wakawidget.data.jobs

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.ForegroundInfo
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.threeten.bp.LocalTime
import ru.devtron.wakawidget.R
import ru.devtron.wakawidget.domain.dashboard.DashInteractor
import ru.devtron.wakawidget.framework.WorkerHelper
import ru.devtron.wakawidget.framework.logAndSaveToFile
import ru.devtron.wakawidget.presentation.widget.WakaWidget

@HiltWorker
class StatsWorker @AssistedInject constructor(
    @Assisted  val context: Context,
    @Assisted workerParameters: WorkerParameters,
    private val dashInteractor: DashInteractor
) : CoroutineWorker(context, workerParameters) {

    private suspend fun heavyWork() = withContext(Dispatchers.IO) {
        val now = LocalTime.now()
        logAndSaveToFile(
            "Doing background work! " + now.hour + ":" +
                    now.minute + ":" + now.second + ":" + now.nano
        )

        dashInteractor.reload()

        updateWakaWidget(applicationContext)
        val afterTime = LocalTime.now()
        logAndSaveToFile(
            "Background work is done! Sent broadCast to widget "
                    + afterTime.hour + ":" + afterTime.minute + ":" + afterTime.second + ":" + afterTime.nano
        )
    }

    private fun isWifiConnected(): Boolean {
        // From https://stackoverflow.com/a/34576776/4418073
        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworks = connectivityManager.allNetworks
        for (network in activeNetworks) {
            if (connectivityManager.getNetworkInfo(network)?.isConnected == true) {
                val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
                if (networkCapabilities?.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) == true) {
                    return true
                }
            }
        }
        return false
    }

    companion object {
        fun updateWakaWidget(applicationContext: Context) {
            val intent = Intent(applicationContext, WakaWidget::class.java)
            intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
            // Use an array and EXTRA_APPWIDGET_IDS instead of AppWidgetManager.EXTRA_APPWIDGET_ID,
            // since it seems the onUpdate() is only fired on that:
            val ids = AppWidgetManager.getInstance(applicationContext)
                .getAppWidgetIds(ComponentName(applicationContext, WakaWidget::class.java))
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            applicationContext.sendBroadcast(intent)
        }
    }

    override suspend fun doWork(): Result {
        return try {
            setForeground(createForegroundInfo())
            if (inputData.getBoolean(WorkerHelper.WIFI, false) && !isWifiConnected()) {
                logAndSaveToFile("SyncWorker: wifi is not connected. Try again next time..")
            } else {
                logAndSaveToFile("SyncWorker: heavy work..")
                heavyWork()
            }

            Result.success()
        } catch (throwable: Throwable) {
            logAndSaveToFile("doWork Failure ${throwable.message}")
            throwable.printStackTrace()
            Result.failure()
        }
    }

    private fun createNotification(): Notification {
        val intent = WorkManager.getInstance(context).createCancelPendingIntent(id)

        val builder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID_SYNC)
            .setContentTitle(context.getString(R.string.sync_works_notification_message))
            .setSmallIcon(R.drawable.ic_baseline_sync_24)
            .setContentIntent(intent)
            .setOngoing(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel().also {
                builder.setChannelId(it.id)
            }
        }
        return builder.build()
    }

    private fun createForegroundInfo() = ForegroundInfo(NOTIFICATION_ID, createNotification())

    @TargetApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): NotificationChannel {
        return NotificationChannel(
            NOTIFICATION_CHANNEL_ID_SYNC,
            context.getString(R.string.sync_channel_name),
            NotificationManager.IMPORTANCE_LOW
        ).also { channel ->
            NotificationManagerCompat.from(context).createNotificationChannel(channel)
        }
    }
}

private const val NOTIFICATION_CHANNEL_ID_SYNC = "sync_channel"
private const val NOTIFICATION_ID = 1001
