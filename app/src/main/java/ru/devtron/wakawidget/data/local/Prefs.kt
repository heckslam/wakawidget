package ru.devtron.wakawidget.data.local

import com.chibatching.kotpref.KotprefModel

object User : KotprefModel() {
    var userId by stringPref()
    var token by stringPref()
    var dialogAutoStartAppShown by booleanPref()
}

object Worker : KotprefModel() {
    var workerDelay by intPref(default = 60)
    var wifiOnly by booleanPref()
    var charginOnly by booleanPref()
    var batteryNotLow by booleanPref()
    var deviceIdle by booleanPref()
}