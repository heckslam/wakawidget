package ru.devtron.wakawidget.data.entity.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass
import ru.devtron.wakawidget.data.entity.res.DurationRes
import ru.devtron.wakawidget.domain.dashboard.models.BestDay
import ru.devtron.wakawidget.domain.dashboard.models.StatsItem

@Entity
@JsonClass(generateAdapter = true)
data class StatsEntity(
    val stats: StatsModelEntity,
    val lastUpdatedTime: String,
    @PrimaryKey
    val id: Int = 1
)

@JsonClass(generateAdapter = true)
data class StatsModelEntity(
    val bestDay: BestDay?,
    val dailyAverage: Int,
    val totalSec: Int,
    val projects: List<StatsItem>?,
    val languages: List<StatsItem>?,
    val editors: List<StatsItem>?,
    val operatingSystems: List<StatsItem>?,
    val range: String,
    val lastUpdated: Long,
    val isEmpty: Boolean,
    val durations: List<DurationRes>,
    val improvementPercent: Double = 0.0,
    val rank: Int? = null
) {
    val todayDuration: Int
        get() = durations.sumByDouble { it.duration }.toInt()
}



