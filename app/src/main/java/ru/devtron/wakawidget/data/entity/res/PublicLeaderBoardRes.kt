package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PublicLeaderBoardRes(
    @Json(name = "current_user")
    val currentUser: PublicUserRes?
)
