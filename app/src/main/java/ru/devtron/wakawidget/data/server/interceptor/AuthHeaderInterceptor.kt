package ru.devtron.wakawidget.data.server.interceptor

import android.util.Base64
import okhttp3.Interceptor
import okhttp3.Response
import ru.devtron.wakawidget.BuildConfig
import ru.devtron.wakawidget.data.local.User

class AuthHeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = if (User.token.isNotEmpty()) {
            request.newBuilder().addHeader("Authorization", "Bearer " + User.token)
                .addHeader("Accept", "application/json").build()
        } else {
            val credentials = BuildConfig.WAKA_CLIENT_ID + ":" + BuildConfig.WAKA_CLIENT_SECRET
            val authorization =
                "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
            request.newBuilder().addHeader("Authorization", authorization)
                .addHeader("Accept", "application/json").build()
        }
        return chain.proceed(request)
    }
}
