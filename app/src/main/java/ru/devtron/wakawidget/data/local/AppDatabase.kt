package ru.devtron.wakawidget.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import ru.devtron.wakawidget.data.entity.db.LeaderBoardsEntity
import ru.devtron.wakawidget.data.entity.db.LeadersBoardEntity
import ru.devtron.wakawidget.data.entity.db.StatsEntity
import ru.devtron.wakawidget.data.entity.db.StatsModelEntity
import ru.devtron.wakawidget.data.local.daos.IPrivateLeaderBoards
import ru.devtron.wakawidget.data.local.daos.IStats
import java.lang.reflect.ParameterizedType

@Database(entities = [StatsEntity::class, LeaderBoardsEntity::class], version = 4, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun statsDao(): IStats
    abstract fun leaderBoardsDao(): IPrivateLeaderBoards
}

class Converters {
    @TypeConverter
    fun toStatsModel(value: String): StatsModelEntity = fromJson(value)

    @TypeConverter
    fun fromStatsModel(value: StatsModelEntity): String = toJson(value)

    @TypeConverter
    fun toLeaderBoardModel(value: String): List<LeadersBoardEntity> = fromJsonL(value)

    @TypeConverter
    fun fromLeaderBoardModel(value: List<LeadersBoardEntity>): String = toJsonL(value)
}

inline fun <reified E> Moshi.listAdapter(elementType: Class<E> = E::class.java): JsonAdapter<List<E>> {
    return adapter(listType<E>(elementType))
}

inline fun <reified E> listType(elementType: Class<E> = E::class.java): ParameterizedType {
    return Types.newParameterizedType(List::class.java, elementType::class.java)
}

inline fun <reified T> fromJson(value: String): T {
    val moshi = Moshi.Builder().build()
    val jsonAdapter = moshi.adapter(T::class.java)
    return jsonAdapter.fromJson(value)!!
}

inline fun <reified T> toJson(value: T): String {
    val moshi = Moshi.Builder().build()
    val jsonAdapter = moshi.adapter(T::class.java)
    return jsonAdapter.toJson(value)
}

inline fun <reified T> toJsonL(value: List<T>): String {
    val moshi = Moshi.Builder().build()

    val type = Types.newParameterizedType(List::class.java, T::class.java)
    val jsonAdapter = moshi.adapter<List<T>>(type)
    return jsonAdapter.toJson(value)
}

inline fun <reified T> fromJsonL(value: String): List<T> {
    val moshi = Moshi.Builder().build()
    val type = Types.newParameterizedType(List::class.java, T::class.java)
    val jsonAdapter = moshi.adapter<List<T>>(type)
    return jsonAdapter.fromJson(value)!!
}
