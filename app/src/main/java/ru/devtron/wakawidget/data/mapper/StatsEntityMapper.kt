package ru.devtron.wakawidget.data.mapper

import ru.devtron.wakawidget.data.entity.db.StatsModelEntity
import ru.devtron.wakawidget.domain.dashboard.models.StatsModel

fun StatsModel.toStatsModelEntity() = StatsModelEntity(
    bestDay,
    dailyAverage,
    totalSec,
    projects,
    languages,
    editors,
    operatingSystems,
    range,
    lastUpdated,
    isEmpty,
    durations,
    improvementPercent,
    rank
)
