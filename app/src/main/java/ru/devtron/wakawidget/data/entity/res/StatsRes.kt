package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class StatsRes(
    @Json(name = "best_day")
    val bestDay: BestDayRes?,
    @Json(name = "daily_average")
    val dailyAverage: Int,
    val editors: List<Editor>?,
    val languages: List<Language>?,
    @Json(name = "operating_systems")
    val operatingSystems: List<OperatingSystem>?,
    val projects: List<Project>?,
    val range: String,
    @Json(name = "total_seconds")
    val totalSeconds: Float?
)
