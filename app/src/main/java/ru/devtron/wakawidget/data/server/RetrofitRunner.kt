package ru.devtron.wakawidget.data.server

import retrofit2.Response
import ru.devtron.wakawidget.framework.extensions.bodyOrThrow
import ru.devtron.wakawidget.framework.extensions.toException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitRunner @Inject constructor() {
    suspend fun <T : Any> executeForResponse(request: suspend () -> Response<T>): Result<T> {
        return try {
            val response = request()
            if (response.isSuccessful) {
                Result.Success(response.bodyOrThrow())
            } else {
                Result.Error(response.toException())
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Result.Error(e)
        }
    }
}
