package ru.devtron.wakawidget.data.server

import retrofit2.http.*
import ru.devtron.wakawidget.data.entity.res.*

interface WakatimeApi {

    @FormUrlEncoded
    @POST("https://wakatime.com/oauth/token")
    suspend fun auth(
        @Field("code") code: String,
        @Field("redirect_uri") redirectUri: String,
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") type: String = "authorization_code"
    ): TokenDataRes

    @FormUrlEncoded
    @POST("https://wakatime.com/oauth/token")
    suspend fun refreshToken(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("redirect_uri") redirectUri: String,
        @Field("grant_type") grantType: String,
        @Field("refresh_token") refreshToken: String
    ): TokenDataRes

    @GET("users/current/stats/{range}")
    suspend fun getCurrentUserStats(
        @Path("range") range: String
    ): StatsWrapperRes

    @GET("users/current/durations")
    suspend fun getDurations(@Query("date") date: String): DurationWrapperRes

    @GET("users/current/goals")
    suspend fun getGoals(): GoalsWrapperRes

    @GET("leaders")
    suspend fun getPublicRank(): PublicLeaderBoardRes

    @GET("users/current/leaderboards")
    suspend fun getPrivateLeaderBoards(): PrivateLeadersBoardsRes

    @GET("/api/v1/users/current/leaderboards/{boardId}")
    suspend fun getPrivateLeaders(
        @Path("boardId") boardId: String
    ): PrivateLeadersRes
}
