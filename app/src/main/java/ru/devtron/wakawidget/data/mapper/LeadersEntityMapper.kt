package ru.devtron.wakawidget.data.mapper

import ru.devtron.wakawidget.data.entity.db.LeaderBoardsEntity
import ru.devtron.wakawidget.data.entity.db.LeadersBoardEntity
import ru.devtron.wakawidget.domain.dashboard.models.LeaderBoards

fun List<LeaderBoards>.toLeaderboardsModelEntity() = LeaderBoardsEntity(
    map {
        LeadersBoardEntity(
            it.name,
            it.leaders
        )
    }
)
