package ru.devtron.wakawidget.data.entity.res

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class StatsWrapperRes(
    @Json(name = "data")
    val statsRes: StatsRes
)
