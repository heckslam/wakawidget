package ru.devtron.wakawidget

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.content.FileProvider
import java.io.File
import java.util.*

private const val FILE_PROVIDER_AUTHORITY = "ru.devtron.wakawidget.fileprovider"

fun sendFilesByEmail(context: Context, files: Array<File?>) {
    val uriList = ArrayList<Uri>()
    for (file in files) {
        uriList.add(
            FileProvider.getUriForFile(
                context.applicationContext,
                FILE_PROVIDER_AUTHORITY,
                file!!
            )
        )
    }
    val intent = Intent(Intent.ACTION_SEND_MULTIPLE)
        .setType("vnd.android.cursor.dir/email")
        .putParcelableArrayListExtra(Intent.EXTRA_STREAM, uriList)
        .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_ACTIVITY_NO_HISTORY)
    if (intent.resolveActivity(context.packageManager) != null) {
        context.startActivity(intent)
    } else {
        Toast.makeText(
            context,
            context.getString(R.string.common_email_sending_not_found),
            Toast.LENGTH_LONG
        ).show()
    }
}
