package ru.devtron.wakawidget

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import com.chibatching.kotpref.Kotpref
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber
import javax.inject.Inject


@HiltAndroidApp
class WakaApplication : Application(), Configuration.Provider {
    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    override fun onCreate() {
        super.onCreate()

        instance = this

        Kotpref.init(this)
        Timber.plant(Timber.DebugTree())

        AndroidThreeTen.init(this@WakaApplication)

        if (BuildConfig.DEBUG) {
            FileSendManager.init(
                this,
                FileManager
            )
        }

    }

    companion object {
        @get:Synchronized
        lateinit var instance: WakaApplication
            private set
    }

    override fun getWorkManagerConfiguration() = Configuration.Builder()
        .setMinimumLoggingLevel(android.util.Log.DEBUG)
        .setWorkerFactory(workerFactory)
        .build()
}
