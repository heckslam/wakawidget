# Waka.Widget (work-in-progress 👷🔧️👷‍⛏)

**The app uses unstable libraries as a testing polygon**

Redree.waka is an application that will help analyze the time spent on coding, WakaTime.
The main function of this application is to show minimal information about your activity in the form of a widget on your desktop, with the ability to adjust the frequency of synchronization and manual updates

## Android development

Redree.waka attempts to use the latest cutting edge libraries and tools. As a summary:

 * Entirely written in [Kotlin](https://kotlinlang.org/)
 * Uses [Kotlin Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) ***(NO RX)***
 * Uses all of the [Architecture Components](https://developer.android.com/topic/libraries/architecture/): Room, LiveData and Lifecycle-components
 * Uses [Toothpick](https://github.com/stephanenicolas/toothpick) for dependency injection

**But also:**
 * Navigation Architecture Component 1.0.0-alpha06
 * WorkManager 1.0.0-alpha09
 
Above libs are still in **alpha** so there may be bugs

For **architecture** uses something remotely resembling [MvRx](https://github.com/airbnb/MvRx/wiki) from Airbnb, but without Rx and different updating mechanism

## Roadmap
 * Write more Unit tests
 * Redesign settings
 * Redesign about screen
 * Add filtering options

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png">](https://play.google.com/store/apps/details?id=ru.devtron.wakawidget)

Application  | Widget
------------- | -------------
![app](/screenshots/1.jpg)  | ![widget](/screenshots/2.jpg)

### API keys

You need to supply API / client keys for the various services the
app uses. That is currently [wakatime.com](https://wakatime.com/developers) and [Fabric](https://fabric.io) (for Crashlytics). You can find information about
how to gain access via the relevant links.

When you obtain the keys, you can provide them to the app by putting the following in the
`secret.properties` file in your user home (create if not exist):

```
# Get these from Wakatime
waka_client_id=<insert>
waka_client_secret=<insert>
waka_callback_url=<insert>
```

## Contributions

If you've found an error in this sample, please file an issue.

Patches are encouraged, and may be submitted by forking this project and
submitting a pull request. Since this project is still in its very early stages,
if your change is substantial, please raise an issue first to discuss it.

## License

```
Copyright (c) 2018 Ruslan Aliev

Licensed to the Apache Software Foundation (ASF) under one or more contributor
license agreements. See the NOTICE file distributed with this work for
additional information regarding copyright ownership. The ASF licenses this
file to you under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.
```